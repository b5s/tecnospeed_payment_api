<?php

namespace B5STecnologia\TecnospeedPaymentAPI;

use B5STecnologia\TecnospeedPaymentAPI\Infra\Exceptions\InvalidValueException;

class Beneficiary extends Tecnospeed implements \JsonSerializable
{

	/**
	 * @param string|null $name
	 * @param string|null $cpfCnpj
	 * @param string|null $bankCode
	 * @param string|null $agency
	 * @param string|null $agencyDigit
	 * @param string|null $accountOperation
	 * @param string|null $accountNumber
	 * @param string|null $accountNumberDigit
	 * @param string|null $street
	 * @param string|null $neighborhood
	 * @param string|null $addressNumber
	 * @param string|null $addressComplement
	 * @param string|null $city
	 * @param string|null $state
	 * @param string|null $zipcode
	 * @param int|null $accountType
	 * @param int|null $transferOptions
	 * @param string|null $accountDac
	 */
	public function __construct(
		public ?string $name = null,
		public ?string $cpfCnpj = null,
		public ?string $bankCode = null,
		public ?string $agency = null,
		public ?string $agencyDigit = null,
		public ?string $accountOperation = null,
		public ?string $accountNumber = null,
		public ?string $accountNumberDigit = null,
		public ?string $street = null,
		public ?string $neighborhood = null,
		public ?string $addressNumber = null,
		public ?string $addressComplement = null,
		public ?string $city = null,
		public ?string $state = null,
		public ?string $zipcode = null,
		public ?int $accountType = null,
		public ?int $transferOptions = null,
		public ?string $accountDac = null
	){
	}

	/**
	 * @return string|null
	 */
	public function getName(): ?string
	{
		return $this->name;
	}

	/**
	 * @param string|null $name
	 * @return Beneficiary
	 */
	public function setName(?string $name): Beneficiary
	{
		$this->name = $name;
		return $this;
	}

	/**
	 * @return string|null
	 */
	public function getCpfCnpj(): ?string
	{
		return $this->cpfCnpj;
	}

	/**
	 * @param string|null $cpfCnpj
	 * @return Beneficiary
	 */
	public function setCpfCnpj(?string $cpfCnpj): Beneficiary
	{
		$this->cpfCnpj = $cpfCnpj;
		return $this;
	}

	/**
	 * @return string|null
	 */
	public function getBankCode(): ?string
	{
		return $this->bankCode;
	}

	/**
	 * @param string|null $bankCode
	 * @return Beneficiary
	 */
	public function setBankCode(?string $bankCode): Beneficiary
	{
		$this->bankCode = $bankCode;
		return $this;
	}

	/**
	 * @return string|null
	 */
	public function getAgency(): ?string
	{
		return $this->agency;
	}

	/**
	 * @param string|null $agency
	 * @return Beneficiary
	 */
	public function setAgency(?string $agency): Beneficiary
	{
		$this->agency = $agency;
		return $this;
	}

	/**
	 * @return string|null
	 */
	public function getAgencyDigit(): ?string
	{
		return $this->agencyDigit;
	}

	/**
	 * @param string|null $agencyDigit
	 * @return Beneficiary
	 */
	public function setAgencyDigit(?string $agencyDigit): Beneficiary
	{
		$this->agencyDigit = $agencyDigit;
		return $this;
	}

	/**
	 * @return string|null
	 */
	public function getAccountOperation(): ?string
	{
		return $this->accountOperation;
	}

	/**
	 * @param string|null $accountOperation
	 * @return Beneficiary
	 */
	public function setAccountOperation(?string $accountOperation): Beneficiary
	{
		$this->accountOperation = $accountOperation;
		return $this;
	}

	/**
	 * @return string|null
	 */
	public function getAccountNumber(): ?string
	{
		return $this->accountNumber;
	}

	/**
	 * @param string|null $accountNumber
	 * @return Beneficiary
	 */
	public function setAccountNumber(?string $accountNumber): Beneficiary
	{
		$this->accountNumber = $accountNumber;
		return $this;
	}

	/**
	 * @return string|null
	 */
	public function getAccountNumberDigit(): ?string
	{
		return $this->accountNumberDigit;
	}

	/**
	 * @param string|null $accountNumberDigit
	 * @return Beneficiary
	 */
	public function setAccountNumberDigit(?string $accountNumberDigit): Beneficiary
	{
		$this->accountNumberDigit = $accountNumberDigit;
		return $this;
	}

	/**
	 * @return string|null
	 */
	public function getStreet(): ?string
	{
		return $this->street;
	}

	/**
	 * @param string|null $street
	 * @return Beneficiary
	 */
	public function setStreet(?string $street): Beneficiary
	{
		$this->street = $street;
		return $this;
	}

	/**
	 * @return string|null
	 */
	public function getNeighborhood(): ?string
	{
		return $this->neighborhood;
	}

	/**
	 * @param string|null $neighborhood
	 * @return Beneficiary
	 */
	public function setNeighborhood(?string $neighborhood): Beneficiary
	{
		$this->neighborhood = $neighborhood;
		return $this;
	}

	/**
	 * @return string|null
	 */
	public function getAddressNumber(): ?string
	{
		return $this->addressNumber;
	}

	/**
	 * @param string|null $addressNumber
	 * @return Beneficiary
	 */
	public function setAddressNumber(?string $addressNumber): Beneficiary
	{
		$this->addressNumber = $addressNumber;
		return $this;
	}

	/**
	 * @return string|null
	 */
	public function getAddressComplement(): ?string
	{
		return $this->addressComplement;
	}

	/**
	 * @param string|null $addressComplement
	 * @return Beneficiary
	 */
	public function setAddressComplement(?string $addressComplement): Beneficiary
	{
		$this->addressComplement = $addressComplement;
		return $this;
	}

	/**
	 * @return string|null
	 */
	public function getCity(): ?string
	{
		return $this->city;
	}

	/**
	 * @param string|null $city
	 * @return Beneficiary
	 */
	public function setCity(?string $city): Beneficiary
	{
		$this->city = $city;
		return $this;
	}

	/**
	 * @return string|null
	 */
	public function getState(): ?string
	{
		return $this->state;
	}

	/**
	 * @param string|null $state
	 * @return Beneficiary
	 */
	public function setState(?string $state): Beneficiary
	{
		$this->state = $state;
		return $this;
	}

	/**
	 * @return string|null
	 */
	public function getZipcode(): ?string
	{
		return $this->zipcode;
	}

	/**
	 * @param string|null $zipcode
	 * @return Beneficiary
	 */
	public function setZipcode(?string $zipcode): Beneficiary
	{
		$this->zipcode = $zipcode;
		return $this;
	}

	/**
	 * @return int|null
	 */
	public function getAccountType(): ?int
	{
		return $this->accountType;
	}

	/**
	 * @param int|null $accountType
	 * @return Beneficiary
	 */
	public function setAccountType(?int $accountType): Beneficiary
	{
		$this->accountType = $accountType;
		return $this;
	}

	/**
	 * @return int|null
	 */
	public function getTransferOptions(): ?int
	{
		return $this->transferOptions;
	}

	/**
	 * @param int|null $transferOptions
	 * @return Beneficiary
	 */
	public function setTransferOptions(?int $transferOptions): Beneficiary
	{
		$this->transferOptions = $transferOptions;
		return $this;
	}

	/**
	 * @return string|null
	 */
	public function getAccountDac(): ?string
	{
		return $this->accountDac;
	}

	/**
	 * @param string|null $accountDac
	 * @return Beneficiary
	 */
	public function setAccountDac(?string $accountDac): Beneficiary
	{
		$this->accountDac = $accountDac;
		return $this;
	}


	/**
	 * @return array
	 */
	public function jsonSerialize():array{
		return [
			'name'     => $this->getName(),
			'cpfCnpj'  => $this->getCpfCnpj(),
			'bankCode' => $this->getBankCode(),
			'agency'   => $this->getAgency(),
			'agencyDigit' => $this->getAgencyDigit(),
			'accountOperation' => $this->getAccountOperation(),
			'accountNumber' => $this->getAccountNumber(),
			'accountNumberDigit' => $this->getAccountNumberDigit(),
			'street' => $this->getStreet(),
			'neighborhood' => $this->getNeighborhood(),
			'addressNumber' => $this->getAddressNumber(),
			'addressComplement' => $this->getAddressComplement(),
			'city' => $this->getCity(),
			'state' => $this->getState(),
			'zipcode' => $this->getZipcode(),
			'accountType' => $this->getAccountType(),
			'transferOptions' => $this->getTransferOptions(),
			'accountDac' => $this->getAccountDac()
		];
	}



	/**
	 * @return void
	 * @throws InvalidValueException
	 */
	public function validateMandatoryFields():void{
		$requiredFields = ["name", "cpfCnpj"];
		foreach ($requiredFields as $field){
			if($this->{$field} === null) {
				throw new InvalidValueException(message: "$field field is mandatory");
			}
		}
	}
}