<?php

namespace B5STecnologia\TecnospeedPaymentAPI;

use B5STecnologia\TecnospeedPaymentAPI\Infra\Exceptions\UnauthenticatedException;
use B5STecnologia\TecnospeedPaymentAPI\Payment\BankTransfer;
use B5STecnologia\TecnospeedPaymentAPI\Payment\Billet;
use B5STecnologia\TecnospeedPaymentAPI\Payment\Various;

class Tecnospeed{
	protected static string $accessToken;
	protected static string $accessCpfCnpj;
	protected static string $baseUrl = "https://staging.pagamentobancario.com.br/api/v1/";

	/**
	 * @param string $accessToken
	 * @param string $cpfCnpj
	 * @param bool $isProduction
	 * @return void
	 */
	final public static function setCredentials(string $accessToken, string $cpfCnpj, bool $isProduction = false):void{
		self::$accessToken = $accessToken;
		self::$accessCpfCnpj = $cpfCnpj;

		if($isProduction){
			self::$baseUrl = "https://api.pagamentobancario.com.br/api/v1/";
		}
	}

	/**
	 * @return void
	 * @throws UnauthenticatedException
	 */
	protected static function verifyCredentials():void{
		if(!isset(self::$accessCpfCnpj, self::$accessToken)){
			throw new UnauthenticatedException("Access Token/CpfCnpj not informed. Please call ". __CLASS__ . "::setCredentials");
		}
	}
	/**
	 * @return Payer
	 */
	public static function getNewPayer():Payer{
		return new Payer();
	}

	/**
	 * @return Account
	 */
	public static function getNewAccount():Account{
		return new Account();
	}

	/**
	 * @return BankTransfer
	 */
	public static function getNewBankTransfer():BankTransfer{
		return new BankTransfer();
	}

	/**
	 * @return Beneficiary
	 */
	public static function getNewBeneficiary():Beneficiary{
		return new Beneficiary();
	}

	/**
	 * @return Payment
	 */
	public static function getNewPayment():Payment {
		return new Payment();
	}

	/**
	 * @return Billet
	 */
	public static function getNewBillet():Billet{
		return new Billet();
	}

	/**
	 * @return Remittance
	 */
	public static function getNewRemittance():Remittance{
		return new Remittance();
	}

	/**
	 * @return Reconciliation
	 */
	public static function getNewReconciliation():Reconciliation{
		return new Reconciliation();
	}

	/**
	 * @return Various
	 */
	public static function  getNewVarious():Various{
		return new Various();
	}
}