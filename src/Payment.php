<?php

namespace B5STecnologia\TecnospeedPaymentAPI;


use GuzzleHttp\Exception\GuzzleException;

class Payment extends Tecnospeed implements \JsonSerializable
{

	/**
	 * @param string|null $uniqueId
	 * @param string|null $status
	 * @param string|null $accountHash
	 * @param int|null $paymentType
	 * @param string|null $paymentForm
	 * @param string|null $avalistaName
	 * @param string|null $avalistaCpfCnpj
	 * @param int|null $compromiseType
	 * @param int|null $transmissionParam
	 * @param int|null $installmentForm
	 * @param int|null $periodicDueDate
	 * @param Beneficiary|null $beneficiary
	 * @param string|null $paymentDate
	 * @param string|null $dueDate
	 * @param float|null $amount
	 * @param float|null $interestAmount
	 * @param string|null $barcode
	 * @param string|null $digitableLine
	 * @param string|null $revenueCode
	 * @param string|null $referencePeriod
	 * @param array|null $occurrences
	 * @param string|null $authenticationRegister
	 * @param array|null $tags
	 * @param string|null $createdAt
	 * @param array|null $paymentsDeleted
	 */
	public function __construct(
		private ?string $uniqueId = null,
		private ?string $status = null,
		private ?string $accountHash = null,
		private ?int $paymentType = null,
		private ?string $paymentForm = null,
		private ?string $avalistaName = null,
		private ?string $avalistaCpfCnpj = null,
		private ?int $compromiseType  = null,
		private ?int $transmissionParam  = null,
		private ?int $installmentForm  = null,
		private ?int $periodicDueDate  = null,
		private ?Beneficiary $beneficiary  = null,
		private ?string $paymentDate = null,
		private ?string $dueDate  = null ,
		private ?float $amount  = null,
		private ?float $interestAmount   = null,
		private ?string $barcode = null,
		private ?string $digitableLine = null,
		private ?string $revenueCode = null,
		private ?string $referencePeriod = null,
		private ?array $occurrences = null,
		private ?string $authenticationRegister = null,
		private ?array $tags = null,
		private ?string $createdAt = null,
		private ?array $paymentsDeleted = null
	){
	}

	/**
	 * @return string|null
	 */
	public function getUniqueId(): ?string
	{
		return $this->uniqueId;
	}

	/**
	 * @param string|null $uniqueId
	 * @return Payment
	 */
	public function setUniqueId(?string $uniqueId): Payment
	{
		$this->uniqueId = $uniqueId;
		return $this;
	}

	/**
	 * @return string|null
	 */
	public function getStatus(): ?string
	{
		return $this->status;
	}

	/**
	 * @param string|null $status
	 * @return Payment
	 */
	public function setStatus(?string $status): Payment
	{
		$this->status = $status;
		return $this;
	}

	/**
	 * @return string|null
	 */
	public function getAccountHash(): ?string
	{
		return $this->accountHash;
	}

	/**
	 * @param string|null $accountHash
	 * @return Payment
	 */
	public function setAccountHash(?string $accountHash): Payment
	{
		$this->accountHash = $accountHash;
		return $this;
	}

	/**
	 * @return int|null
	 */
	public function getPaymentType(): ?int
	{
		return $this->paymentType;
	}

	/**
	 * @param int|null $paymentType
	 * @return Payment
	 */
	public function setPaymentType(?int $paymentType): Payment
	{
		$this->paymentType = $paymentType;
		return $this;
	}

	/**
	 * @return string|null
	 */
	public function getPaymentForm(): ?string
	{
		return $this->paymentForm;
	}

	/**
	 * @param string|null $paymentForm
	 * @return Payment
	 */
	public function setPaymentForm(?string $paymentForm): Payment
	{
		$this->paymentForm = $paymentForm;
		return $this;
	}

	/**
	 * @return string|null
	 */
	public function getAvalistaName(): ?string
	{
		return $this->avalistaName;
	}

	/**
	 * @param string|null $avalistaName
	 * @return Payment
	 */
	public function setAvalistaName(?string $avalistaName): Payment
	{
		$this->avalistaName = $avalistaName;
		return $this;
	}

	/**
	 * @return string|null
	 */
	public function getAvalistaCpfCnpj(): ?string
	{
		return $this->avalistaCpfCnpj;
	}

	/**
	 * @param string|null $avalistaCpfCnpj
	 * @return Payment
	 */
	public function setAvalistaCpfCnpj(?string $avalistaCpfCnpj): Payment
	{
		$this->avalistaCpfCnpj = $avalistaCpfCnpj;
		return $this;
	}

	/**
	 * @return int|null
	 */
	public function getCompromiseType(): ?int
	{
		return $this->compromiseType;
	}

	/**
	 * @param int|null $compromiseType
	 * @return Payment
	 */
	public function setCompromiseType(?int $compromiseType): Payment
	{
		$this->compromiseType = $compromiseType;
		return $this;
	}

	/**
	 * @return int|null
	 */
	public function getTransmissionParam(): ?int
	{
		return $this->transmissionParam;
	}

	/**
	 * @param int|null $transmissionParam
	 * @return Payment
	 */
	public function setTransmissionParam(?int $transmissionParam): Payment
	{
		$this->transmissionParam = $transmissionParam;
		return $this;
	}

	/**
	 * @return int|null
	 */
	public function getInstallmentForm(): ?int
	{
		return $this->installmentForm;
	}

	/**
	 * @param int|null $installmentForm
	 * @return Payment
	 */
	public function setInstallmentForm(?int $installmentForm): Payment
	{
		$this->installmentForm = $installmentForm;
		return $this;
	}

	/**
	 * @return int|null
	 */
	public function getPeriodicDueDate(): ?int
	{
		return $this->periodicDueDate;
	}

	/**
	 * @param int|null $periodicDueDate
	 * @return Payment
	 */
	public function setPeriodicDueDate(?int $periodicDueDate): Payment
	{
		$this->periodicDueDate = $periodicDueDate;
		return $this;
	}

	/**
	 * @return Beneficiary|null
	 */
	public function getBeneficiary(): ?Beneficiary
	{
		return $this->beneficiary;
	}

	/**
	 * @param Beneficiary|null $beneficiary
	 * @return Payment
	 */
	public function setBeneficiary(?Beneficiary $beneficiary): Payment
	{
		$this->beneficiary = $beneficiary;
		return $this;
	}

	/**
	 * @return string|null
	 */
	public function getPaymentDate(): ?string
	{
		return $this->paymentDate;
	}

	/**
	 * @param string|null $paymentDate
	 * @return Payment
	 */
	public function setPaymentDate(?string $paymentDate): Payment
	{
		$this->paymentDate = $paymentDate;
		return $this;
	}

	/**
	 * @return string|null
	 */
	public function getDueDate(): ?string
	{
		return $this->dueDate;
	}

	/**
	 * @param string|null $dueDate
	 * @return Payment
	 */
	public function setDueDate(?string $dueDate): Payment
	{
		$this->dueDate = $dueDate;
		return $this;
	}

	/**
	 * @return float|null
	 */
	public function getAmount(): ?float
	{
		return $this->amount;
	}

	/**
	 * @param float|null $amount
	 * @return Payment
	 */
	public function setAmount(?float $amount): Payment
	{
		$this->amount = $amount;
		return $this;
	}

	/**
	 * @return float|null
	 */
	public function getInterestAmount(): ?float
	{
		return $this->interestAmount;
	}

	/**
	 * @param float|null $interestAmount
	 * @return Payment
	 */
	public function setInterestAmount(?float $interestAmount): Payment
	{
		$this->interestAmount = $interestAmount;
		return $this;
	}

	/**
	 * @return string|null
	 */
	public function getBarcode(): ?string
	{
		return $this->barcode;
	}

	/**
	 * @param string|null $barcode
	 * @return Payment
	 */
	public function setBarcode(?string $barcode): Payment
	{
		$this->barcode = $barcode;
		return $this;
	}

	/**
	 * @return string|null
	 */
	public function getDigitableLine(): ?string
	{
		return $this->digitableLine;
	}

	/**
	 * @param string|null $digitableLine
	 * @return Payment
	 */
	public function setDigitableLine(?string $digitableLine): Payment
	{
		$this->digitableLine = $digitableLine;
		return $this;
	}

	/**
	 * @return string|null
	 */
	public function getRevenueCode(): ?string
	{
		return $this->revenueCode;
	}

	/**
	 * @param string|null $revenueCode
	 * @return Payment
	 */
	public function setRevenueCode(?string $revenueCode): Payment
	{
		$this->revenueCode = $revenueCode;
		return $this;
	}

	/**
	 * @return string|null
	 */
	public function getReferencePeriod(): ?string
	{
		return $this->referencePeriod;
	}

	/**
	 * @param string|null $referencePeriod
	 * @return Payment
	 */
	public function setReferencePeriod(?string $referencePeriod): Payment
	{
		$this->referencePeriod = $referencePeriod;
		return $this;
	}

	/**
	 * @return array|null
	 */
	public function getOccurrences(): ?array
	{
		return $this->occurrences;
	}

	/**
	 * @param array|null $occurrences
	 * @return Payment
	 */
	public function setOccurrences(?array $occurrences): Payment
	{
		$this->occurrences = $occurrences;
		return $this;
	}

	/**
	 * @return string|null
	 */
	public function getAuthenticationRegister(): ?string
	{
		return $this->authenticationRegister;
	}

	/**
	 * @param string|null $authenticationRegister
	 * @return Payment
	 */
	public function setAuthenticationRegister(?string $authenticationRegister): Payment
	{
		$this->authenticationRegister = $authenticationRegister;
		return $this;
	}

	/**
	 * @return array|null
	 */
	public function getTags(): ?array
	{
		return $this->tags;
	}

	/**
	 * @param array|null $tags
	 * @return Payment
	 */
	public function setTags(?array $tags): Payment
	{
		$this->tags = $tags;
		return $this;
	}

	/**
	 * @return string|null
	 */
	public function getCreatedAt(): ?string
	{
		return $this->createdAt;
	}

	/**
	 * @param string|null $createdAt
	 * @return Payment
	 */
	public function setCreatedAt(?string $createdAt): Payment
	{
		$this->createdAt = $createdAt;
		return $this;
	}

	/**
	 * @return array|null
	 */
	public function getPaymentsDeleted(): ?array
	{
		return $this->paymentsDeleted;
	}

	/**
	 * @param array|null $paymentsDeleted
	 * @return Payment
	 */
	public function setPaymentsDeleted(?array $paymentsDeleted): Payment
	{
		$this->paymentsDeleted = $paymentsDeleted;
		return $this;
	}



	/**
	 * @return array
	 */
	public function jsonSerialize():array{
		return [
			'uniqueId' => $this->getUniqueId(),
			'status'   => $this->getStatus(),
			'accountHash' => $this->getAccountHash(),
			'paymentType' => $this->getPaymentType(),
			'paymentForm' => $this->getPaymentForm(),
			'avalistaName' => $this->getAvalistaName(),
			'avalistaCpfCnpj' => $this->getAvalistaCpfCnpj(),
			'compromiseType' => $this->getCompromiseType(),
			'transmissionParam' => $this->getTransmissionParam(),
			'installmentForm' => $this->getInstallmentForm(),
			'periodicDueDate' => $this->getPeriodicDueDate(),
			'beneficiary' => $this->getBeneficiary(),
			'paymentDate' => $this->getPaymentDate(),
			'dueDate' => $this->getDueDate(),
			'amount' => $this->getAmount(),
			'interestAmount' => $this->getInterestAmount(),
			'barcode' => $this->getBarcode(),
			'digitableLine' => $this->getDigitableLine(),
			'revenueCode' => $this->getRevenueCode(),
			'referencePeriod' => $this->getReferencePeriod(),
			'occurences' => $this->getOccurrences(),
			'authenticationRegister' => $this->getAuthenticationRegister(),
			'tags' => $this->getTags(),
			'createdAt' => $this->getCreatedAt(),
			'paymentsDeleted' => $this->getPaymentsDeleted()
		];
	}

	/**
	 * @throws Infra\Exceptions\UnauthenticatedException
	 * @throws Infra\Exceptions\InvalidValueException
	 * @throws Infra\Exceptions\NotFoundException
	 * @throws GuzzleException
	 */
	public function getPayment(string $payerCpfCnpj, string $uniqueIdPayment){
		self::verifyCredentials();
		$request = new Request();
		$responseApi = $request->request(
			methodHttp:'GET',
			url: self::$baseUrl."payment?uniqueId=".$uniqueIdPayment,
			accessToken: self::$accessToken,
			accessCpfCnpj: self::$accessCpfCnpj,
			headers: ['payercpfcnpj' => $payerCpfCnpj]
		);

		$beneficiary = self::getNewBeneficiary();
		$beneficiary->setName($responseApi->beneficiary->name)
			->setCpfCnpj($responseApi->beneficiary->cpfCnpj)
			->setBankCode($responseApi->beneficiary->bankCode)
			->setAgency($responseApi->beneficiary->agency)
			->setAgencyDigit($responseApi->beneficiary->agencyDigit)
			->setAccountOperation($responseApi->beneficiary->accountOperation)
			->setAccountNumber($responseApi->beneficiary->accountNumber)
			->setStreet($responseApi->beneficiary->street)
			->setAccountNumberDigit($responseApi->beneficiary->accountNumberDigit)
			->setAccountDac($responseApi->beneficiary->accountDac)
			->setNeighborhood($responseApi->beneficiary->neighborhood)
			->setAddressNumber($responseApi->beneficiary->addressNumber)
			->setAddressComplement($responseApi->beneficiary->addressComplement)
			->setCity($responseApi->beneficiary->city)
			->setState($responseApi->beneficiary->state)
			->setZipcode($responseApi->beneficiary->zipcode);

		$this->setUniqueId($responseApi->uniqueId)
			->setStatus($responseApi->status)
			->setAccountHash($responseApi->accountHash)
			->setPaymentType($responseApi->paymentType)
			->setPaymentForm($responseApi->paymentForm)
			->setAvalistaName($responseApi->avalistaName)
			->setAvalistaCpfCnpj($responseApi->avalistaCpfCnpj)
			->setCompromiseType($responseApi->compromiseType)
			->setTransmissionParam($responseApi->transmissionParam)
			->setInstallmentForm($responseApi->installmentForm)
			->setPeriodicDueDate($responseApi->periodicDueDate)
			->setBeneficiary($beneficiary)
			->setPaymentDate($responseApi->paymentDate)
			->setDueDate($responseApi->dueDate)
			->setAmount($responseApi->amount)
			->setInterestAmount($responseApi->interestAmount)
			->setBarcode($responseApi->barcode)
			->setDigitableLine($responseApi->digitableLine)
			->setRevenueCode($responseApi->revenueCode)
			->setReferencePeriod($responseApi->referencePeriod)
			->setOccurrences($responseApi->occurrences)
			->setAuthenticationRegister($responseApi->authenticationRegister)
			->setTags($responseApi->tags)
			->setCreatedAt($responseApi->createdAt);

		return $this;
	}

	/**
	 * @param string $payerCpfCnpj
	 * @param array $payments
	 * @return $this
	 * @throws GuzzleException
	 * @throws Infra\Exceptions\InvalidValueException
	 * @throws Infra\Exceptions\NotFoundException
	 * @throws Infra\Exceptions\UnauthenticatedException
	 */
	public function delete(string $payerCpfCnpj, array $payments):Payment{
		self::verifyCredentials();
		$request = new Request();
		$responseApi = $request->request(
			methodHttp:'DELETE',
			url: self::$baseUrl."payment",
			accessToken: self::$accessToken,
			accessCpfCnpj: self::$accessCpfCnpj,
			headers: ['payercpfcnpj' => $payerCpfCnpj],
			body: ["payments" => $payments]
		);

		$this->setStatus($responseApi->status)
			->setPaymentsDeleted($responseApi->payments);

		return $this;
	}
}