<?php

namespace B5STecnologia\TecnospeedPaymentAPI\Infra\Enums;

/**
 * @see https://developer.mozilla.org/en-US/docs/Web/HTTP/Status
 */
enum HttpStatus:int implements EnumInterface
{
	/**
	 * A requisição foi finalizada com sucesso
	 */
	case Success = 200;
	/**
	 * Um novo recurso foi criado com sucesso
	 */
	case Created = 201;
	/**
	 * A requisição foi finalizada com sucesso, mas nenhum dado será devolvido pela API
	 */
	case NoContent = 204;
	/**
	 * Parâmetros inválidos na requisição
	 */
	case BadRequest = 400;
	/**
	 * Não autenticado
	 */
	case Unauthorized = 401;
	/**
	 * Autenticado, porém, sem permissão para executar a ação
	 */
	case Forbiden = 403;
	/**
	 * Item solicitado não foi encontrado
	 */
	case NotFound = 404;
	/**
	 * Parâmetros Faltando
	 */
	case NotAcceptable = 406;
	/**
	 * Erro desconhecido no servidor
	 */
	case InternalError = 500;
	/**
	 * Método não implementado
	 */
	case NotImplemented = 501;

	/**
	 * Erro de gateway
	 */
	case BadGateway = 502;
	/**
	 * Serviço indisponível
	 */
	case ServiceUnavailable = 503;

	public function label():string
	{
		return self::getLabel($this);
	}

	public static function getLabel($value):string
	{
		return match ($value){
			HttpStatus::Success				=> "Sucesso",
			HttpStatus::Created				=> "Criado",
			HttpStatus::NoContent			=> "Sem Conteúdo",
			HttpStatus::BadRequest			=> "Requisição Inválida",
			HttpStatus::Unauthorized		=> "Não Autorizado",
			HttpStatus::Forbiden			=> "Acesso Proibido",
			HttpStatus::NotFound			=> "Não Encontrado",
			HttpStatus::NotAcceptable		=> "Parâmetros Faltando",
			HttpStatus::InternalError		=> "Erro Interno",
			HttpStatus::NotImplemented		=> "Não Implementado",
			HttpStatus::BadGateway          => "Erro de Gateway",
			HttpStatus::ServiceUnavailable	=> "Serviço Indisponível",
		};
	}

	public function getScalarValue():int{
		return match ($this){
			HttpStatus::Success				=> 200,
			HttpStatus::Created				=> 201,
			HttpStatus::NoContent			=> 204,
			HttpStatus::BadRequest			=> 400,
			HttpStatus::Unauthorized		=> 401,
			HttpStatus::Forbiden			=> 403,
			HttpStatus::NotFound			=> 404,
			HttpStatus::NotAcceptable		=> 406,
			HttpStatus::InternalError		=> 500,
			HttpStatus::NotImplemented		=> 501,
			HttpStatus::BadGateway          => 502,
			HttpStatus::ServiceUnavailable	=> 503,
		};
	}
}