<?php

namespace B5STecnologia\TecnospeedPaymentAPI\Infra\Enums;

interface EnumInterface
{
	public function label():string;
	public static function getLabel($value):string;
}