<?php

namespace B5STecnologia\TecnospeedPaymentAPI\Infra;

use B5STecnologia\TecnospeedPaymentAPI\Infra\Exceptions\InvalidValueException;

class Validators
{
	public static function validateCpf(string $cpf): bool
	{
		$cpf = preg_replace('@\D@', '', $cpf);

		if (strlen($cpf) !== 11) {
			return false;
		}

		if (preg_match('@(\d{1})\1{10}@', $cpf)) {
			return false;
		}

		$dv_informado = substr($cpf, 9, 2);

		$soma = 0;
		$multiplicador = 10;

		for ($i = 0; $i < 9; $i++) {
			$numeroatual = $cpf[$i];
			$soma += ($numeroatual * $multiplicador);
			$multiplicador--;
		}

		$dv1_encontrado = $soma % 11;
		if ($dv1_encontrado < 2) {
			$dv1_encontrado = 0;
		} else {
			$dv1_encontrado = 11 - $dv1_encontrado;
		}

		$multiplicador = 11;
		$soma = 0;

		for ($i = 0; $i < 10; $i++) {
			$numeroatual = $cpf[$i];
			$soma += ($numeroatual * $multiplicador);
			$multiplicador--;
		}


		$dv2_encontrado = $soma % 11;
		if ($dv2_encontrado < 2) {
			$dv2_encontrado = 0;
		} else {
			$dv2_encontrado = 11 - $dv2_encontrado;
		}

		$dv_final = $dv1_encontrado . $dv2_encontrado;

		if ($dv_final !== $dv_informado) {
			return false;
		}

		return true;
	}

	public static function validateCnpj(string $cnpj): bool
	{
		$cnpj = preg_replace("/\D/", "", $cnpj);
		if (strlen($cnpj) === 15) {
			$cnpj = substr($cnpj, 1, 14);
		}

		if (strlen($cnpj) !== 14) return false;
		$soma1 = ($cnpj[0] * 5) +
			($cnpj[1] * 4) +
			($cnpj[2] * 3) +
			($cnpj[3] * 2) +
			($cnpj[4] * 9) +
			($cnpj[5] * 8) +
			($cnpj[6] * 7) +
			($cnpj[7] * 6) +
			($cnpj[8] * 5) +
			($cnpj[9] * 4) +
			($cnpj[10] * 3) +
			($cnpj[11] * 2);
		$resto = $soma1 % 11;
		$digito1 = $resto < 2 ? 0 : 11 - $resto;
		$soma2 = ($cnpj[0] * 6) +
			($cnpj[1] * 5) +
			($cnpj[2] * 4) +
			($cnpj[3] * 3) +
			($cnpj[4] * 2) +
			($cnpj[5] * 9) +
			($cnpj[6] * 8) +
			($cnpj[7] * 7) +
			($cnpj[8] * 6) +
			($cnpj[9] * 5) +
			($cnpj[10] * 4) +
			($cnpj[11] * 3) +
			($cnpj[12] * 2);

		$resto = $soma2 % 11;
		$digito2 = $resto < 2 ? 0 : 11 - $resto;
		return (($cnpj[12] == $digito1) && ($cnpj[13] == $digito2));
	}

	/**
	 * @param string $email
	 * @throws InvalidValueException
	 */
	public static function validateEmail(string $email): void
	{
		if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
			throw new InvalidValueException("Email is invalid.");
		}
	}
}