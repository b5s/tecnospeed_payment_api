<?php

namespace B5STecnologia\TecnospeedPaymentAPI\Infra\Exceptions;

use B5STecnologia\TecnospeedPaymentAPI\Infra\Enums\HttpStatus;

class InvalidValueException extends TecnospeedException {

	public function getHttpStatus(): int
	{
		return HttpStatus::BadRequest->getScalarValue();
	}
}