<?php

namespace B5STecnologia\TecnospeedPaymentAPI\Infra\Exceptions;


use Exception;
use Throwable;

abstract class TecnospeedException extends Exception
{
	public function __construct(string $message = "", int $code = 0, ?Throwable $previous = null){
		parent::__construct($message, $code, $previous);
	}

	/**
	 * @return array
	 */
	public function getDetailedErrorMessage():array{
		$exceptionStack = [$this];
		$previous = $this->getPrevious();

		while($previous){
			$exceptionStack[]	= $previous;
			$previous			= $previous->getPrevious();
		}

		$exceptionStack = array_reverse($exceptionStack);

		$detailedMessage = [
			"message"	=> "",
			"trace"		=> $this->getTraceAsString()
		];
		foreach ($exceptionStack as $index=>$exception) {
			$detailedMessage['message'] .= " #" . $index+1 . " " . $exception->getMessage();
		}

		return $detailedMessage;
	}

	abstract public function getHttpStatus():int;

    /**
     * @return Throwable
     */
    public function getOriginalException():Throwable{
        $exception = $this;

        while ($exception->getPrevious()){
            $exception = $exception->getPrevious();
        }

        return $exception;
    }


}