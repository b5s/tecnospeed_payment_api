<?php

namespace B5STecnologia\TecnospeedPaymentAPI\Payment;

use B5STecnologia\TecnospeedPaymentAPI\Beneficiary;
use B5STecnologia\TecnospeedPaymentAPI\Infra\Exceptions\InvalidValueException;
use B5STecnologia\TecnospeedPaymentAPI\Infra\Exceptions\NotFoundException;
use B5STecnologia\TecnospeedPaymentAPI\Infra\Exceptions\UnauthenticatedException;
use B5STecnologia\TecnospeedPaymentAPI\Request;
use B5STecnologia\TecnospeedPaymentAPI\Tecnospeed;
use GuzzleHttp\Exception\GuzzleException;
use JsonSerializable;
use stdClass;

class Various extends Tecnospeed implements JsonSerializable{

	/**
	 * @param string|null $uniqueId
	 * @param string|null $status
	 * @param string|null $accountHash
	 * @param string|null $description
	 * @param string|null $paymentForm
	 * @param string|null $paymentDate
	 * @param string|null $dueDate
	 * @param float|null $amount
	 * @param float|null $rebateAmount
	 * @param float|null $interestAmount
	 * @param float|null $discountAmount
	 * @param float|null $fineAmount
	 * @param string|null $movimentCode
	 * @param string|null $complementaryCode
	 * @param int|null $compromiseType
	 * @param int|null $transmissionParam
	 * @param string|null $pixType
	 * @param string|null $pixKey
	 * @param string|null $ispbCode
	 * @param string|null $registrationComplement
	 * @param string|null $pixUrl
	 * @param string|null $pixTxid
	 * @param Beneficiary|null $beneficiary
	 * @param array|null $tags
	 */
	public function __construct(
		private ?string $uniqueId = null,
		private ?string $status = null,
		private ?string $accountHash = null,
		private ?string $description = null,
		private ?string $paymentForm = null,
		private ?string $paymentDate = null,
		private ?string $dueDate  = null ,
		private ?float $amount  = null,
		private ?float $rebateAmount   = null,
		private ?float $interestAmount   = null,
		private ?float $discountAmount  = null,
		private ?float $fineAmount  = null,
		private ?string $movimentCode  = null,
		private ?string $complementaryCode  = null,
		private ?int $compromiseType  = null,
		private ?int $transmissionParam  = null,
		private ?string $pixType = null,
		private ?string $pixKey = null,
		private ?string $ispbCode = null,
		private ?string $registrationComplement = null,
		private ?string $pixUrl = null,
		private ?string $pixTxid = null,
		private ?Beneficiary $beneficiary  = null,
		private ?array $tags = null
	){}

	/**
	 * @return string|null
	 */
	public function getUniqueId(): ?string
	{
		return $this->uniqueId;
	}

	/**
	 * @param string|null $uniqueId
	 * @return Various
	 */
	public function setUniqueId(?string $uniqueId): Various
	{
		$this->uniqueId = $uniqueId;
		return $this;
	}

	/**
	 * @return string|null
	 */
	public function getStatus(): ?string
	{
		return $this->status;
	}

	/**
	 * @param string|null $status
	 * @return Various
	 */
	public function setStatus(?string $status): Various
	{
		$this->status = $status;
		return $this;
	}

	/**
	 * @return string|null
	 */
	public function getAccountHash(): ?string
	{
		return $this->accountHash;
	}

	/**
	 * @param string|null $accountHash
	 * @return Various
	 */
	public function setAccountHash(?string $accountHash): Various
	{
		$this->accountHash = $accountHash;
		return $this;
	}

	/**
	 * @return string|null
	 */
	public function getDescription(): ?string
	{
		return $this->description;
	}

	/**
	 * @param string|null $description
	 * @return Various
	 */
	public function setDescription(?string $description): Various
	{
		$this->description = $description;
		return $this;
	}

	/**
	 * @return string|null
	 */
	public function getPaymentForm(): ?string
	{
		return $this->paymentForm;
	}

	/**
	 * @param string|null $paymentForm
	 * @return Various
	 */
	public function setPaymentForm(?string $paymentForm): Various
	{
		$this->paymentForm = $paymentForm;
		return $this;
	}

	/**
	 * @return string|null
	 */
	public function getPaymentDate(): ?string
	{
		return $this->paymentDate;
	}

	/**
	 * @param string|null $paymentDate
	 * @return Various
	 */
	public function setPaymentDate(?string $paymentDate): Various
	{
		$this->paymentDate = $paymentDate;
		return $this;
	}

	/**
	 * @return string|null
	 */
	public function getDueDate(): ?string
	{
		return $this->dueDate;
	}

	/**
	 * @param string|null $dueDate
	 * @return Various
	 */
	public function setDueDate(?string $dueDate): Various
	{
		$this->dueDate = $dueDate;
		return $this;
	}

	/**
	 * @return float|null
	 */
	public function getAmount(): ?float
	{
		return $this->amount;
	}

	/**
	 * @param float|null $amount
	 * @return Various
	 */
	public function setAmount(?float $amount): Various
	{
		$this->amount = $amount;
		return $this;
	}

	/**
	 * @return float|null
	 */
	public function getRebateAmount(): ?float
	{
		return $this->rebateAmount;
	}

	/**
	 * @param float|null $rebateAmount
	 * @return Various
	 */
	public function setRebateAmount(?float $rebateAmount): Various
	{
		$this->rebateAmount = $rebateAmount;
		return $this;
	}

	/**
	 * @return float|null
	 */
	public function getInterestAmount(): ?float
	{
		return $this->interestAmount;
	}

	/**
	 * @param float|null $interestAmount
	 * @return Various
	 */
	public function setInterestAmount(?float $interestAmount): Various
	{
		$this->interestAmount = $interestAmount;
		return $this;
	}

	/**
	 * @return float|null
	 */
	public function getDiscountAmount(): ?float
	{
		return $this->discountAmount;
	}

	/**
	 * @param float|null $discountAmount
	 * @return Various
	 */
	public function setDiscountAmount(?float $discountAmount): Various
	{
		$this->discountAmount = $discountAmount;
		return $this;
	}

	/**
	 * @return float|null
	 */
	public function getFineAmount(): ?float
	{
		return $this->fineAmount;
	}

	/**
	 * @param float|null $fineAmount
	 * @return Various
	 */
	public function setFineAmount(?float $fineAmount): Various
	{
		$this->fineAmount = $fineAmount;
		return $this;
	}

	/**
	 * @return string|null
	 */
	public function getMovimentCode(): ?string
	{
		return $this->movimentCode;
	}

	/**
	 * @param string|null $movimentCode
	 * @return Various
	 */
	public function setMovimentCode(?string $movimentCode): Various
	{
		$this->movimentCode = $movimentCode;
		return $this;
	}

	/**
	 * @return string|null
	 */
	public function getComplementaryCode(): ?string
	{
		return $this->complementaryCode;
	}

	/**
	 * @param string|null $complementaryCode
	 * @return Various
	 */
	public function setComplementaryCode(?string $complementaryCode): Various
	{
		$this->complementaryCode = $complementaryCode;
		return $this;
	}

	/**
	 * @return int|null
	 */
	public function getCompromiseType(): ?int
	{
		return $this->compromiseType;
	}

	/**
	 * @param int|null $compromiseType
	 * @return Various
	 */
	public function setCompromiseType(?int $compromiseType): Various
	{
		$this->compromiseType = $compromiseType;
		return $this;
	}

	/**
	 * @return int|null
	 */
	public function getTransmissionParam(): ?int
	{
		return $this->transmissionParam;
	}

	/**
	 * @param int|null $transmissionParam
	 * @return Various
	 */
	public function setTransmissionParam(?int $transmissionParam): Various
	{
		$this->transmissionParam = $transmissionParam;
		return $this;
	}

	/**
	 * @return string|null
	 */
	public function getPixType(): ?string
	{
		return $this->pixType;
	}

	/**
	 * @param string|null $pixType
	 * @return Various
	 */
	public function setPixType(?string $pixType): Various
	{
		$this->pixType = $pixType;
		return $this;
	}

	/**
	 * @return string|null
	 */
	public function getPixKey(): ?string
	{
		return $this->pixKey;
	}

	/**
	 * @param string|null $pixKey
	 * @return Various
	 */
	public function setPixKey(?string $pixKey): Various
	{
		$this->pixKey = $pixKey;
		return $this;
	}

	/**
	 * @return string|null
	 */
	public function getIspbCode(): ?string
	{
		return $this->ispbCode;
	}

	/**
	 * @param string|null $ispbCode
	 * @return Various
	 */
	public function setIspbCode(?string $ispbCode): Various
	{
		$this->ispbCode = $ispbCode;
		return $this;
	}

	/**
	 * @return string|null
	 */
	public function getRegistrationComplement(): ?string
	{
		return $this->registrationComplement;
	}

	/**
	 * @param string|null $registrationComplement
	 * @return Various
	 */
	public function setRegistrationComplement(?string $registrationComplement): Various
	{
		$this->registrationComplement = $registrationComplement;
		return $this;
	}

	/**
	 * @return string|null
	 */
	public function getPixUrl(): ?string
	{
		return $this->pixUrl;
	}

	/**
	 * @param string|null $pixUrl
	 * @return Various
	 */
	public function setPixUrl(?string $pixUrl): Various
	{
		$this->pixUrl = $pixUrl;
		return $this;
	}

	/**
	 * @return string|null
	 */
	public function getPixTxid(): ?string
	{
		return $this->pixTxid;
	}

	/**
	 * @param string|null $pixTxid
	 * @return Various
	 */
	public function setPixTxid(?string $pixTxid): Various
	{
		$this->pixTxid = $pixTxid;
		return $this;
	}

	/**
	 * @return Beneficiary|null
	 */
	public function getBeneficiary(): ?Beneficiary
	{
		return $this->beneficiary;
	}

	/**
	 * @param Beneficiary|null $beneficiary
	 * @return Various
	 */
	public function setBeneficiary(?Beneficiary $beneficiary): Various
	{
		$this->beneficiary = $beneficiary;
		return $this;
	}

	/**
	 * @return array|null
	 */
	public function getTags(): ?array
	{
		return $this->tags;
	}

	/**
	 * @param array|null $tags
	 * @return Various
	 */
	public function setTags(?array $tags): Various
	{
		$this->tags = $tags;
		return $this;
	}



	public function jsonSerialize(): mixed
	{
		return [
			'uniqueId' => $this->getUniqueId(),
			'status' => $this->getStatus(),
			'accountHash' => $this->getAccountHash(),
			'description' => $this->getDescription(),
			'paymentForm' => $this->getPaymentForm(),
			'paymentDate' => $this->getPaymentDate(),
			'dueDate' => $this->getDueDate(),
			'amount' => $this->getAmount(),
			'rebateAmount' => $this->getRebateAmount(),
			'interestAmount' => $this->getInterestAmount(),
			'discountAmount' => $this->getDiscountAmount(),
			'fineAmount'  => $this->getFineAmount(),
			'movimentCode'  => $this->getMovimentCode(),
			'complementaryCode' => $this->getComplementaryCode(),
			'compromiseType' => $this->getCompromiseType(),
			'transmissionParam' => $this->getTransmissionParam(),
			'pixType' => $this->getPixType(),
			'pixKey' => $this->getPixKey(),
			'ispbCode' => $this->getIspbCode(),
			'registrationComplement' => $this->getRegistrationComplement(),
			'pixUrl' => $this->getPixUrl(),
			'pixTxid' => $this->getPixTxid(),
			'beneficiary' => $this->getBeneficiary(),
			'tags' => $this->getTags()
		];
	}

	/**
	 * @return void
	 * @throws InvalidValueException
	 */
	private function validateMandatoryFields():void{
		$requiredFields = ["accountHash", "paymentForm","paymentDate",  "amount", "beneficiary"];
		foreach ($requiredFields as $field){
			if($this->{$field} === null) {
				throw new InvalidValueException(message: "$field field is mandatory");
			}
		}

		$this->beneficiary->validateMandatoryFields();
	}


	/**
	 * @return stdClass
	 */
	private function validateAndCreateObject(): stdClass{
		$newVariousPayment = new stdClass();
		foreach($this as $property => $value){
			if($value !== null){
				$newVariousPayment->{$property} = $this->{$property};
			}
		}

		return $newVariousPayment;
	}

	/**
	 * @param string $payerCpfCnpj
	 * @return $this
	 * @throws InvalidValueException
	 * @throws NotFoundException
	 * @throws UnauthenticatedException
	 * @throws GuzzleException
	 */
	public function create(string $payerCpfCnpj):self{
		self::verifyCredentials();
		$this->validateMandatoryFields();
		$newVariousPayment = $this->validateAndCreateObject();
		$newVariousPayment->beneficiary = $this->removeNullPropertiesFromBeneficiary($newVariousPayment->beneficiary);
		$request = new Request();
		$responseApi = $request->request(
			methodHttp:'POST',
			url: self::$baseUrl."payment/various",
			accessToken: self::$accessToken,
			accessCpfCnpj: self::$accessCpfCnpj,
			headers: ['payercpfcnpj' => $payerCpfCnpj],
			body:  $newVariousPayment
		);

		$beneficiary = $this->getBeneficiary();
		$beneficiary?->setName($responseApi->beneficiary->name)
			->setCpfCnpj($responseApi->beneficiary->cpfCnpj)
			->setBankCode($responseApi->beneficiary->bankCode)
			->setAgency($responseApi->beneficiary->agency)
			->setAgencyDigit($responseApi->beneficiary->agencyDigit)
			->setAccountNumber($responseApi->beneficiary->accountNumber)
			->setAccountNumberDigit($responseApi->beneficiary->accountNumberDigit)
			->setAccountDac($responseApi->beneficiary->accountDac)
			->setStreet($responseApi->beneficiary->street)
			->setNeighborhood($responseApi->beneficiary->neighborhood)
			->setAddressNumber($responseApi->beneficiary->addressNumber)
			->setAddressComplement($responseApi->beneficiary->addressComplement)
			->setCity($responseApi->beneficiary->city)
			->setState($responseApi->beneficiary->state)
			->setZipcode($responseApi->beneficiary->zipcode);


		$this->setUniqueId($responseApi->uniqueId)
			->setStatus($responseApi->status)
			->setAccountHash($responseApi->accountHash)
			->setDescription($responseApi->description)
			->setPaymentForm($responseApi->paymentForm)
			->setPaymentDate($responseApi->paymentDate)
			->setDueDate($responseApi->dueDate)
			->setAmount($responseApi->amount)
			->setRebateAmount($responseApi->rebateAmount)
			->setInterestAmount($responseApi->interestAmount)
			->setDiscountAmount($responseApi->discountAmount)
			->setFineAmount($responseApi->fineAmount)
			->setMovimentCode($responseApi->movimentCode)
			->setComplementaryCode($responseApi->complementaryCode)
			->setCompromiseType($responseApi->compromiseType)
			->setTransmissionParam($responseApi->transmissionParam)
			->setPixType($responseApi->pixType)
			->setPixKey($responseApi->pixKey)
			->setPixUrl($responseApi->pixUrl)
			->setPixTxid($responseApi->pixTxid)
			->setIspbCode($responseApi->ispbCode)
			->setRegistrationComplement($responseApi->registrationComplement)
			->setTags($responseApi->tags);

		return $this;
	}

	/**
	 * @param Beneficiary $beneficiary
	 * @return stdClass
	 */
	private function removeNullPropertiesFromBeneficiary(Beneficiary $beneficiary):stdClass {
		$beneficiaryData = new stdClass();
		foreach ($beneficiary  as $property => $value){
			if(!is_null($value)){
				$beneficiaryData->{$property} = $value;
			}
		}

		return $beneficiaryData;
	}
}