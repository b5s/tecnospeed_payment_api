<?php

namespace B5STecnologia\TecnospeedPaymentAPI\Payment;

use B5STecnologia\TecnospeedPaymentAPI\Beneficiary;
use B5STecnologia\TecnospeedPaymentAPI\Infra\Exceptions\InvalidValueException;
use B5STecnologia\TecnospeedPaymentAPI\Infra\Exceptions\NotFoundException;
use B5STecnologia\TecnospeedPaymentAPI\Infra\Exceptions\UnauthenticatedException;
use B5STecnologia\TecnospeedPaymentAPI\Request;
use B5STecnologia\TecnospeedPaymentAPI\Tecnospeed;
use GuzzleHttp\Exception\GuzzleException;
use JsonSerializable;
use stdClass;

class BankTransfer extends Tecnospeed implements JsonSerializable
{

	/**
	 * @param string|null $uniqueId
	 * @param string|null $status
	 * @param string|null $accountHash
	 * @param string|null $description
	 * @param string|null $paymentForm
	 * @param string|null $paymentDate
	 * @param string|null $dueDate
	 * @param float|null $amount
	 * @param float|null $rebateAmount
	 * @param float|null $interestAmount
	 * @param float|null $discountAmount
	 * @param float|null $fineAmount
	 * @param int|null $compensation
	 * @param string|null $movimentCode
	 * @param string|null $complementaryCode
	 * @param int|null $installmentForm
	 * @param int|null $periodicDueDate
	 * @param int|null $compromiseType
	 * @param int|null $transmissionParam
	 * @param Beneficiary|null $beneficiary
	 * @param array|null $tags
	 */
	public function __construct(
		private ?string $uniqueId = null,
		private ?string $status = null,
		private ?string $accountHash = null,
		private ?string $description = null,
		private ?string $paymentForm = null,
		private ?string $paymentDate = null,
		private ?string $dueDate  = null ,
		private ?float $amount  = null,
		private ?float $rebateAmount   = null,
		private ?float $interestAmount   = null,
		private ?float $discountAmount  = null,
		private ?float $fineAmount  = null,
		private ?int $compensation  = null,
		private ?string $movimentCode  = null,
		private ?string $complementaryCode  = null,
		private ?int $installmentForm  = null,
		private ?int $periodicDueDate  = null,
		private ?int $compromiseType  = null,
		private ?int $transmissionParam  = null,
		private ?Beneficiary $beneficiary  = null,
		private ?array $tags = null
	){
	}

	/**
	 * @return string|null
	 */
	public function getUniqueId(): ?string
	{
		return $this->uniqueId;
	}

	/**
	 * @param string|null $uniqueId
	 * @return BankTransfer
	 */
	public function setUniqueId(?string $uniqueId): BankTransfer
	{
		$this->uniqueId = $uniqueId;
		return $this;
	}

	/**
	 * @return string|null
	 */
	public function getStatus(): ?string
	{
		return $this->status;
	}

	/**
	 * @param string|null $status
	 * @return BankTransfer
	 */
	public function setStatus(?string $status): BankTransfer
	{
		$this->status = $status;
		return $this;
	}

	/**
	 * @return string|null
	 */
	public function getAccountHash(): ?string
	{
		return $this->accountHash;
	}

	/**
	 * @param string|null $accountHash
	 * @return BankTransfer
	 */
	public function setAccountHash(?string $accountHash): BankTransfer
	{
		$this->accountHash = $accountHash;
		return $this;
	}

	/**
	 * @return string|null
	 */
	public function getDescription(): ?string
	{
		return $this->description;
	}

	/**
	 * @param string|null $description
	 * @return BankTransfer
	 */
	public function setDescription(?string $description): BankTransfer
	{
		$this->description = $description;
		return $this;
	}

	/**
	 * @return string|null
	 */
	public function getPaymentForm(): ?string
	{
		return $this->paymentForm;
	}

	/**
	 * @param string|null $paymentForm
	 * @return BankTransfer
	 */
	public function setPaymentForm(?string $paymentForm): BankTransfer
	{
		$this->paymentForm = $paymentForm;
		return $this;
	}

	/**
	 * @return string|null
	 */
	public function getPaymentDate(): ?string
	{
		return $this->paymentDate;
	}

	/**
	 * @param string|null $paymentDate
	 * @return BankTransfer
	 */
	public function setPaymentDate(?string $paymentDate): BankTransfer
	{
		$this->paymentDate = $paymentDate;
		return $this;
	}

	/**
	 * @return string|null
	 */
	public function getDueDate(): ?string
	{
		return $this->dueDate;
	}

	/**
	 * @param string|null $dueDate
	 * @return BankTransfer
	 */
	public function setDueDate(?string $dueDate): BankTransfer
	{
		$this->dueDate = $dueDate;
		return $this;
	}

	/**
	 * @return float|null
	 */
	public function getAmount(): ?float
	{
		return $this->amount;
	}

	/**
	 * @param float|null $amount
	 * @return BankTransfer
	 */
	public function setAmount(?float $amount): BankTransfer
	{
		$this->amount = $amount;
		return $this;
	}

	/**
	 * @return float|null
	 */
	public function getRebateAmount(): ?float
	{
		return $this->rebateAmount;
	}

	/**
	 * @param float|null $rebateAmount
	 * @return BankTransfer
	 */
	public function setRebateAmount(?float $rebateAmount): BankTransfer
	{
		$this->rebateAmount = $rebateAmount;
		return $this;
	}

	/**
	 * @return float|null
	 */
	public function getInterestAmount(): ?float
	{
		return $this->interestAmount;
	}

	/**
	 * @param float|null $interestAmount
	 * @return BankTransfer
	 */
	public function setInterestAmount(?float $interestAmount): BankTransfer
	{
		$this->interestAmount = $interestAmount;
		return $this;
	}

	/**
	 * @return float|null
	 */
	public function getDiscountAmount(): ?float
	{
		return $this->discountAmount;
	}

	/**
	 * @param float|null $discountAmount
	 * @return BankTransfer
	 */
	public function setDiscountAmount(?float $discountAmount): BankTransfer
	{
		$this->discountAmount = $discountAmount;
		return $this;
	}

	/**
	 * @return float|null
	 */
	public function getFineAmount(): ?float
	{
		return $this->fineAmount;
	}

	/**
	 * @param float|null $fineAmount
	 * @return BankTransfer
	 */
	public function setFineAmount(?float $fineAmount): BankTransfer
	{
		$this->fineAmount = $fineAmount;
		return $this;
	}

	/**
	 * @return int|null
	 */
	public function getCompensation(): ?int
	{
		return $this->compensation;
	}

	/**
	 * @param int|null $compensation
	 * @return BankTransfer
	 */
	public function setCompensation(?int $compensation): BankTransfer
	{
		$this->compensation = $compensation;
		return $this;
	}

	/**
	 * @return string|null
	 */
	public function getMovimentCode(): ?string
	{
		return $this->movimentCode;
	}

	/**
	 * @param string|null $movimentCode
	 * @return BankTransfer
	 */
	public function setMovimentCode(?string $movimentCode): BankTransfer
	{
		$this->movimentCode = $movimentCode;
		return $this;
	}

	/**
	 * @return string|null
	 */
	public function getComplementaryCode(): ?string
	{
		return $this->complementaryCode;
	}

	/**
	 * @param string|null $complementaryCode
	 * @return BankTransfer
	 */
	public function setComplementaryCode(?string $complementaryCode): BankTransfer
	{
		$this->complementaryCode = $complementaryCode;
		return $this;
	}

	/**
	 * @return int|null
	 */
	public function getInstallmentForm(): ?int
	{
		return $this->installmentForm;
	}

	/**
	 * @param int|null $installmentForm
	 * @return BankTransfer
	 */
	public function setInstallmentForm(?int $installmentForm): BankTransfer
	{
		$this->installmentForm = $installmentForm;
		return $this;
	}

	/**
	 * @return int|null
	 */
	public function getPeriodicDueDate(): ?int
	{
		return $this->periodicDueDate;
	}

	/**
	 * @param int|null $periodicDueDate
	 * @return BankTransfer
	 */
	public function setPeriodicDueDate(?int $periodicDueDate): BankTransfer
	{
		$this->periodicDueDate = $periodicDueDate;
		return $this;
	}

	/**
	 * @return int|null
	 */
	public function getCompromiseType(): ?int
	{
		return $this->compromiseType;
	}

	/**
	 * @param int|null $compromiseType
	 * @return BankTransfer
	 */
	public function setCompromiseType(?int $compromiseType): BankTransfer
	{
		$this->compromiseType = $compromiseType;
		return $this;
	}

	/**
	 * @return int|null
	 */
	public function getTransmissionParam(): ?int
	{
		return $this->transmissionParam;
	}

	/**
	 * @param int|null $transmissionParam
	 * @return BankTransfer
	 */
	public function setTransmissionParam(?int $transmissionParam): BankTransfer
	{
		$this->transmissionParam = $transmissionParam;
		return $this;
	}

	/**
	 * @return Beneficiary|null
	 */
	public function getBeneficiary(): ?Beneficiary
	{
		return $this->beneficiary;
	}

	/**
	 * @param Beneficiary|null $beneficiary
	 * @return BankTransfer
	 */
	public function setBeneficiary(?Beneficiary $beneficiary): BankTransfer
	{
		$this->beneficiary = $beneficiary;
		return $this;
	}

	/**
	 * @return array|null
	 */
	public function getTags(): ?array
	{
		return $this->tags;
	}

	/**
	 * @param array|null $tags
	 * @return BankTransfer
	 */
	public function setTags(?array $tags): BankTransfer
	{
		$this->tags = $tags;
		return $this;
	}


	/**
	 * @return array
	 */
	public function jsonSerialize():array
	{
		return [
			'uniqueId' => $this->getUniqueId(),
			'status' => $this->getStatus(),
			'accountHash' => $this->getAccountHash(),
			'description' => $this->getDescription(),
			'paymentForm' => $this->getPaymentForm(),
			'paymentDate' => $this->getPaymentDate(),
			'dueDate' => $this->getDueDate(),
			'amount' => $this->getAmount(),
			'rebateAmount' => $this->getRebateAmount(),
			'interestAmount' => $this->getInterestAmount(),
			'discountAmount' => $this->getDiscountAmount(),
			'fineAmount'  => $this->getFineAmount(),
			'compensation' => $this->getCompensation(),
			'movimentCode'  => $this->getMovimentCode(),
			'complementaryCode' => $this->getComplementaryCode(),
			'installmentForm' => $this->getInstallmentForm(),
			'periodicDueDate' => $this->getPeriodicDueDate(),
			'compromiseType' => $this->getCompromiseType(),
			'transmissionParam' => $this->getTransmissionParam(),
			'beneficiary' => $this->getBeneficiary(),
			'tags' => $this->getTags()
		];

	}

	/**
	 * @return void
	 * @throws InvalidValueException
	 */
	private function validateMandatoryFields():void{
		$requiredFields = ["accountHash", "paymentForm", "amount", "beneficiary"];
		foreach ($requiredFields as $field){
			if($this->{$field} === null) {
				throw new InvalidValueException(message: "$field field is mandatory");
			}
		}

		$this->beneficiary->validateMandatoryFields();
	}

	/**
	 * @return stdClass
	 */
	private function validateAndCreateObject(): stdClass{
		$newBankTransfer = new stdClass();
		foreach($this as $property => $value){
			if($value !== null){
				$newBankTransfer->{$property} = $this->{$property};
			}
		}

		return $newBankTransfer;
	}

	/**
	 * @return $this
	 * @throws InvalidValueException
	 * @throws UnauthenticatedException
	 * @throws NotFoundException
	 * @throws GuzzleException
	 */
	public function create(string $payerCpfCnpj):self{
		self::verifyCredentials();
		$this->validateMandatoryFields();
		$newBankTransfer = $this->validateAndCreateObject();
		$request = new Request();
		$responseApi = $request->request(
			methodHttp:'POST',
			url: self::$baseUrl."payment/transfer",
			accessToken: self::$accessToken,
			accessCpfCnpj: self::$accessCpfCnpj,
			headers: ['payercpfcnpj' => $payerCpfCnpj],
			body:  $newBankTransfer
		);

		$beneficiary = $this->getBeneficiary();
		$beneficiary?->setName($responseApi->beneficiary->name)
			->setCpfCnpj($responseApi->beneficiary->cpfCnpj)
			->setBankCode($responseApi->beneficiary->bankCode)
			->setAgency($responseApi->beneficiary->agency)
			->setAgencyDigit($responseApi->beneficiary->agencyDigit)
			->setAccountOperation($responseApi->beneficiary->accountOperation)
			->setAccountNumber($responseApi->beneficiary->accountNumber)
			->setAccountNumberDigit($responseApi->beneficiary->accountNumberDigit)
			->setAccountDac($responseApi->beneficiary->accountDac)
			->setStreet($responseApi->beneficiary->street)
			->setNeighborhood($responseApi->beneficiary->neighborhood)
			->setAddressNumber($responseApi->beneficiary->addressNumber)
			->setAddressComplement($responseApi->beneficiary->addressComplement)
			->setCity($responseApi->beneficiary->city)
			->setState($responseApi->beneficiary->state)
			->setZipcode($responseApi->beneficiary->zipcode)
			->setAccountType($responseApi->beneficiary->accountType)
			->setTransferOptions($responseApi->beneficiary->transferOptions);

		$this->setUniqueId($responseApi->uniqueId)
			->setStatus($responseApi->status)
			->setAccountHash($responseApi->accountHash)
			->setDescription($responseApi->description)
			->setPaymentDate($responseApi->paymentDate)
			->setAmount($responseApi->amount)
			->setRebateAmount($responseApi->rebateAmount)
			->setInterestAmount($responseApi->interestAmount)
			->setDiscountAmount($responseApi->discountAmount)
			->setFineAmount($responseApi->fineAmount)
			->setCompensation($responseApi->compensation)
			->setMovimentCode($responseApi->movimentCode)
			->setComplementaryCode($responseApi->complementaryCode)
			->setInstallmentForm($responseApi->installmentForm)
			->setPeriodicDueDate($responseApi->periodicDueDate)
			->setCompromiseType($responseApi->compromiseType)
			->setTransmissionParam($responseApi->transmissionParam)
			->setBeneficiary($beneficiary)
			->setTags($responseApi->tags);

		return $this;
	}
}