<?php

namespace B5STecnologia\TecnospeedPaymentAPI\Payment;

use B5STecnologia\TecnospeedPaymentAPI\Beneficiary;
use B5STecnologia\TecnospeedPaymentAPI\Infra\Exceptions\InvalidValueException;
use B5STecnologia\TecnospeedPaymentAPI\Infra\Exceptions\NotFoundException;
use B5STecnologia\TecnospeedPaymentAPI\Infra\Exceptions\UnauthenticatedException;
use B5STecnologia\TecnospeedPaymentAPI\Request;
use B5STecnologia\TecnospeedPaymentAPI\Tecnospeed;
use GuzzleHttp\Exception\GuzzleException;
use JsonSerializable;
use stdClass;

class Billet extends Tecnospeed implements JsonSerializable{

	/**
	 * @param string|null $uniqueId
	 * @param string|null $status
	 * @param string|null $accountHash
	 * @param int|null $paymentType
	 * @param string|null $paymentForm
	 * @param string|null $description
	 * @param string|null $barcode
	 * @param string|null $dueDate
	 * @param string|null $paymentDate
	 * @param float|null $nominalAmount
	 * @param float|null $discountAmount
	 * @param float|null $feeAmount
	 * @param float|null $amount
	 * @param string|null $movimentCode
	 * @param string|null $avalistaName
	 * @param string|null $avalistaCpfCnpj
	 * @param int|null $compromiseType
	 * @param int|null $transmissionParam
	 * @param Beneficiary|null $beneficiary
	 * @param array|null $tags
	 */
	public function __construct(
		private ?string $uniqueId = null,
		private ?string $status = null,
		private ?string $accountHash = null,
		private ?int    $paymentType = null,
		private ?string $paymentForm = null,
		private ?string $description = null,
		private ?string $barcode = null,
		private ?string $dueDate = null,
		private ?string $paymentDate = null,
		private ?float  $nominalAmount = null,
		private ?float  $discountAmount = null,
		private ?float  $feeAmount = null,
		private ?float  $amount = null,
		private ?string $movimentCode = null,
		private ?string $avalistaName = null,
		private ?string $avalistaCpfCnpj = null,
		private ?int    $compromiseType = null,
		private ?int    $transmissionParam = null,
		private ?Beneficiary $beneficiary = null,
		private ?array $tags = null
	){
	}

	/**
	 * @return string|null
	 */
	public function getUniqueId(): ?string
	{
		return $this->uniqueId;
	}

	/**
	 * @param string|null $uniqueId
	 * @return Billet
	 */
	public function setUniqueId(?string $uniqueId): Billet
	{
		$this->uniqueId = $uniqueId;
		return $this;
	}

	/**
	 * @return string|null
	 */
	public function getStatus(): ?string
	{
		return $this->status;
	}

	/**
	 * @param string|null $status
	 * @return Billet
	 */
	public function setStatus(?string $status): Billet
	{
		$this->status = $status;
		return $this;
	}

	/**
	 * @return string|null
	 */
	public function getAccountHash(): ?string
	{
		return $this->accountHash;
	}

	/**
	 * @param string|null $accountHash
	 * @return Billet
	 */
	public function setAccountHash(?string $accountHash): Billet
	{
		$this->accountHash = $accountHash;
		return $this;
	}

	/**
	 * @return int|null
	 */
	public function getPaymentType(): ?int
	{
		return $this->paymentType;
	}

	/**
	 * @param int|null $paymentType
	 * @return Billet
	 */
	public function setPaymentType(?int $paymentType): Billet
	{
		$this->paymentType = $paymentType;
		return $this;
	}

	/**
	 * @return string|null
	 */
	public function getPaymentForm(): ?string
	{
		return $this->paymentForm;
	}

	/**
	 * @param string|null $paymentForm
	 * @return Billet
	 */
	public function setPaymentForm(?string $paymentForm): Billet
	{
		$this->paymentForm = $paymentForm;
		return $this;
	}

	/**
	 * @return string|null
	 */
	public function getDescription(): ?string
	{
		return $this->description;
	}

	/**
	 * @param string|null $description
	 * @return Billet
	 */
	public function setDescription(?string $description): Billet
	{
		$this->description = $description;
		return $this;
	}

	/**
	 * @return string|null
	 */
	public function getBarcode(): ?string
	{
		return $this->barcode;
	}

	/**
	 * @param string|null $barcode
	 * @return Billet
	 */
	public function setBarcode(?string $barcode): Billet
	{
		$this->barcode = $barcode;
		return $this;
	}

	/**
	 * @return string|null
	 */
	public function getDueDate(): ?string
	{
		return $this->dueDate;
	}

	/**
	 * @param string|null $dueDate
	 * @return Billet
	 */
	public function setDueDate(?string $dueDate): Billet
	{
		$this->dueDate = $dueDate;
		return $this;
	}

	/**
	 * @return string|null
	 */
	public function getPaymentDate(): ?string
	{
		return $this->paymentDate;
	}

	/**
	 * @param string|null $paymentDate
	 * @return Billet
	 */
	public function setPaymentDate(?string $paymentDate): Billet
	{
		$this->paymentDate = $paymentDate;
		return $this;
	}

	/**
	 * @return float|null
	 */
	public function getNominalAmount(): ?float
	{
		return $this->nominalAmount;
	}

	/**
	 * @param float|null $nominalAmount
	 * @return Billet
	 */
	public function setNominalAmount(?float $nominalAmount): Billet
	{
		$this->nominalAmount = $nominalAmount;
		return $this;
	}

	/**
	 * @return float|null
	 */
	public function getDiscountAmount(): ?float
	{
		return $this->discountAmount;
	}

	/**
	 * @param float|null $discountAmount
	 * @return Billet
	 */
	public function setDiscountAmount(?float $discountAmount): Billet
	{
		$this->discountAmount = $discountAmount;
		return $this;
	}

	/**
	 * @return float|null
	 */
	public function getFeeAmount(): ?float
	{
		return $this->feeAmount;
	}

	/**
	 * @param float|null $feeAmount
	 * @return Billet
	 */
	public function setFeeAmount(?float $feeAmount): Billet
	{
		$this->feeAmount = $feeAmount;
		return $this;
	}

	/**
	 * @return float|null
	 */
	public function getAmount(): ?float
	{
		return $this->amount;
	}

	/**
	 * @param float|null $amount
	 * @return Billet
	 */
	public function setAmount(?float $amount): Billet
	{
		$this->amount = $amount;
		return $this;
	}

	/**
	 * @return string|null
	 */
	public function getMovimentCode(): ?string
	{
		return $this->movimentCode;
	}

	/**
	 * @param string|null $movimentCode
	 * @return Billet
	 */
	public function setMovimentCode(?string $movimentCode): Billet
	{
		$this->movimentCode = $movimentCode;
		return $this;
	}

	/**
	 * @return string|null
	 */
	public function getAvalistaName(): ?string
	{
		return $this->avalistaName;
	}

	/**
	 * @param string|null $avalistaName
	 * @return Billet
	 */
	public function setAvalistaName(?string $avalistaName): Billet
	{
		$this->avalistaName = $avalistaName;
		return $this;
	}

	/**
	 * @return string|null
	 */
	public function getAvalistaCpfCnpj(): ?string
	{
		return $this->avalistaCpfCnpj;
	}

	/**
	 * @param string|null $avalistaCpfCnpj
	 * @return Billet
	 */
	public function setAvalistaCpfCnpj(?string $avalistaCpfCnpj): Billet
	{
		$this->avalistaCpfCnpj = $avalistaCpfCnpj;
		return $this;
	}

	/**
	 * @return int|null
	 */
	public function getCompromiseType(): ?int
	{
		return $this->compromiseType;
	}

	/**
	 * @param int|null $compromiseType
	 * @return Billet
	 */
	public function setCompromiseType(?int $compromiseType): Billet
	{
		$this->compromiseType = $compromiseType;
		return $this;
	}

	/**
	 * @return int|null
	 */
	public function getTransmissionParam(): ?int
	{
		return $this->transmissionParam;
	}

	/**
	 * @param int|null $transmissionParam
	 * @return Billet
	 */
	public function setTransmissionParam(?int $transmissionParam): Billet
	{
		$this->transmissionParam = $transmissionParam;
		return $this;
	}

	/**
	 * @return Beneficiary|null
	 */
	public function getBeneficiary(): ?Beneficiary
	{
		return $this->beneficiary;
	}

	/**
	 * @param Beneficiary|null $beneficiary
	 * @return Billet
	 */
	public function setBeneficiary(?Beneficiary $beneficiary): Billet
	{
		$this->beneficiary = $beneficiary;
		return $this;
	}

	/**
	 * @return array|null
	 */
	public function getTags(): ?array
	{
		return $this->tags;
	}

	/**
	 * @param array|null $tags
	 * @return Billet
	 */
	public function setTags(?array $tags): Billet
	{
		$this->tags = $tags;
		return $this;
	}

	/**
	 * @return array
	 */
	public function jsonSerialize():array{
		return [
			'uniqueId' => $this->getUniqueId(),
			'status' => $this->getStatus(),
			'accountHash' => $this->getAccountHash(),
			'description' => $this->getDescription(),
			'paymentType' => $this->getPaymentType(),
			'paymentForm' => $this->getPaymentForm(),
			'barcode' => $this->getBarcode(),
			'dueDate' => $this->getDueDate(),
			'paymentDate' => $this->getPaymentDate(),
			'nominalAmount' => $this->getNominalAmount(),
			'discountAmount' => $this->getDiscountAmount(),
			'feeAmount' => $this->getFeeAmount(),
			'amount' => $this->getAmount(),
			'movimentCode' => $this->getMovimentCode(),
			'avalistaName' => $this->getAvalistaName(),
			'avalistaCpfCnpj' => $this->getAvalistaCpfCnpj(),
			'compromiseType' => $this->getCompromiseType(),
			'transmissionParam' => $this->getTransmissionParam(),
			'beneficiary' => $this->getBeneficiary(),
			'tags' => $this->getTags()
		];
	}

	/**
	 * @return void
	 * @throws InvalidValueException
	 */
	private function validateMandatoryFields():void{
		$requiredFields = ["accountHash", "paymentForm", "barcode", "dueDate", "nominalAmount", "beneficiary"];
		foreach ($requiredFields as $field){
			if($this->{$field} === null) {
				throw new InvalidValueException(message: "$field field is mandatory");
			}
		}

		$this->beneficiary->validateMandatoryFields();
	}

	/**
	 * @return stdClass
	 */
	private function validateAndCreateObject(): stdClass{
		$newBillet = new stdClass();
		foreach($this as $property => $value){
			if($value !== null){
				$newBillet->{$property} = $this->{$property};
			}
		}

		return $newBillet;
	}


	/**
	 * @return $this
	 * @throws InvalidValueException
	 * @throws UnauthenticatedException
	 * @throws NotFoundException
	 * @throws GuzzleException
	 */
	public function create(string $payerCpfCnpj):self{
		self::verifyCredentials();
		$this->validateMandatoryFields();
		$newBillet = $this->validateAndCreateObject();
		$request = new Request();
		$responseApi = $request->request(
			methodHttp:'POST',
			url: self::$baseUrl."payment/billet",
			accessToken: self::$accessToken,
			accessCpfCnpj: self::$accessCpfCnpj,
			headers: ['payercpfcnpj' => $payerCpfCnpj],
			body:  $newBillet
		);

		$beneficiary = $this->getBeneficiary();
		$beneficiary?->setName($responseApi->beneficiary->name)
			->setCpfCnpj($responseApi->beneficiary->cpfCnpj);

		$this->setUniqueId($responseApi->uniqueId)
			->setStatus($responseApi->status)
			->setAccountHash($responseApi->accountHash)
			->setDescription($responseApi->description)
			->setPaymentType($responseApi->paymentType)
			->setPaymentForm($responseApi->paymentForm)
			->setBarcode($responseApi->barcode)
			->setDueDate($responseApi->dueDate)
			->setPaymentDate($responseApi->paymentDate)
			->setNominalAmount($responseApi->nominalAmount)
			->setDiscountAmount($responseApi->discountAmount)
			->setFeeAmount($responseApi->feeAmount)
			->setAmount($responseApi->amount)
			->setMovimentCode($responseApi->movimentCode)
			->setAvalistaName($responseApi->avalistaName)
			->setAvalistaCpfCnpj($responseApi->avalistaCpfCnpj)
			->setCompromiseType($responseApi->compromiseType)
			->setTransmissionParam($responseApi->transmissionParam)
			->setBeneficiary($beneficiary)
			->setTags($responseApi->tags);

		return $this;
	}

}