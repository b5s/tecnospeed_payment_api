<?php

namespace B5STecnologia\TecnospeedPaymentAPI;


use  B5STecnologia\TecnospeedPaymentAPI\Infra\Exceptions\InvalidValueException;
use B5STecnologia\TecnospeedPaymentAPI\Infra\Exceptions\NotFoundException;
use B5STecnologia\TecnospeedPaymentAPI\Infra\Exceptions\UnauthenticatedException;
use GuzzleHttp\Exception\GuzzleException;
use JsonSerializable;
use B5STecnologia\TecnospeedPaymentAPI\Infra\Validators;

class Payer extends Tecnospeed implements JsonSerializable {

	/**
	 * @param string|null $name
	 * @param string|null $cpfCnpj
	 * @param Account[]|null $accounts
	 * @param string|null $neighborhood
	 * @param string|null $city
	 * @param string|null $state
	 * @param string|null $zipcode
	 * @param string|null $addressNumber
	 * @param string|null $addressComplement
	 * @param string|null $street
	 * @param string|null $email
	 * @param string|null $token
	 * @param int|null $status
	 */
	public function __construct(
		private ?string $name = null,
		private ?string $cpfCnpj = null,
		private ?array  $accounts = null,
		private ?string $neighborhood = null,
		private ?string $city = null,
		private ?string $state = null,
		private ?string $zipcode= null,
		private ?string $addressNumber = null,
		private ?string $addressComplement = null,
		private ?string $street = null,
		private ?string $email = null,
		private ?string $token = null,
		private ?int $status = null,
	){

	}

	/**
	 * @return string|null
	 */
	public function getName(): ?string
	{
		return $this->name;
	}

	/**
	 * @param string|null $name
	 * @return $this
	 */
	public function setName(?string $name):self
	{
		$this->name = $name;
		return $this;
	}

	/**
	 * @return string|null
	 */
	public function getCpfCnpj(): ?string
	{
		return $this->cpfCnpj;
	}

	/**
	 * @param string|null $cpfCnpj
	 * @return $this
	 * @throws InvalidValueException
	 */
	public function setCpfCnpj(?string $cpfCnpj):self
	{
		if(!Validators::validateCpf($cpfCnpj) && !Validators::validateCnpj($cpfCnpj)){
			throw new InvalidValueException("CPF/CNPJ is invalid");
		}

		$this->cpfCnpj = $cpfCnpj;
		return $this;
	}

	/**
	 * @return array|null
	 */
	public function getAccounts(): ?array
	{
		return $this->accounts;
	}

	/**
	 * @param array|null $accounts
	 * @return $this
	 */
	public function setAccounts(?array $accounts):self
	{
		$this->accounts = $accounts;
		return $this;
	}

	/**
	 * @return string|null
	 */
	public function getNeighborhood(): ?string
	{
		return $this->neighborhood;
	}

	/**
	 * @param string|null $neighborhood
	 * @return $this
	 */
	public function setNeighborhood(?string $neighborhood):self
	{
		$this->neighborhood = $neighborhood;
		return $this;
	}

	/**
	 * @return string|null
	 */
	public function getCity(): ?string
	{
		return $this->city;
	}

	/**
	 * @param string|null $city
	 * @return $this
	 */
	public function setCity(?string $city):self
	{
		$this->city = $city;
		return $this;
	}

	/**
	 * @return string|null
	 */
	public function getState(): ?string
	{
		return $this->state;
	}

	/**
	 * @param string|null $state
	 * @return $this
	 */
	public function setState(?string $state):self
	{
		$this->state = $state;
		return $this;
	}

	/**
	 * @return string|null
	 */
	public function getZipcode(): ?string
	{
		return $this->zipcode;
	}

	/**
	 * @param string|null $zipcode
	 * @return $this
	 */
	public function setZipcode(?string $zipcode):self
	{
		$this->zipcode = $zipcode;
		return $this;
	}

	/**
	 * @return string|null
	 */
	public function getAddressNumber(): ?string
	{
		return $this->addressNumber;
	}

	/**
	 * @param string|null $addressNumber
	 * @return $this
	 */
	public function setAddressNumber(?string $addressNumber):self
	{
		$this->addressNumber = $addressNumber;
		return $this;
	}

	/**
	 * @return string|null
	 */
	public function getAddressComplement(): ?string
	{
		return $this->addressComplement;
	}

	/**
	 * @param string|null $addressComplement
	 * @return $this
	 */
	public function setAddressComplement(?string $addressComplement):self
	{
		$this->addressComplement = $addressComplement;
		return $this;
	}

	/**
	 * @return string|null
	 */
	public function getStreet(): ?string
	{
		return $this->street;
	}

	/**
	 * @param string|null $street
	 * @return $this
	 */
	public function setStreet(?string $street):self
	{
		$this->street = $street;
		return $this;
	}

	/**
	 * @return string|null
	 */
	public function getEmail(): ?string
	{
		return $this->email;
	}

	/**
	 * @param string|null $email
	 * @return $this
	 * @throws InvalidValueException
	 */
	public function setEmail(?string $email):self
	{
		if(!empty($email)){
			Validators::validateEmail($email);
		}

		$this->email = $email;
		return $this;
	}

	/**
	 * @return string|null
	 */
	public function getToken(): ?string
	{
		return $this->token;
	}

	/**
	 * @param string|null $token
	 * @return $this
	 */
	public function setToken(?string $token):self
	{
		$this->token = $token;
		return $this;
	}

	/**
	 * @return int|null
	 */
	public function getStatus(): ?int
	{
		return $this->status;
	}

	/**
	 * @param int|null $status
	 * @return $this
	 */
	public function setStatus(?int $status):self
	{
		$this->status = $status;
		return $this;
	}


	/**
	 * @return array
	 */
	public function jsonSerialize():array
	{
		$data = [
			'name'      => $this->getName(),
			'cpfCnpj'   => $this->getCpfCnpj(),
			'accounts'  => $this->getAccounts(),
			'neighborhood' => $this->getNeighborhood(),
			'city'      => $this->getCity(),
			'state'     => $this->getState(),
			'zipcode'   => $this->getZipcode(),
			'addressNumber' => $this->getAddressNumber(),
			'addressComplement' => $this->getAddressComplement(),
			'street'    => $this->getStreet(),
			'token'     => $this->getToken(),
			'status'    => $this->getStatus()
		];

		if(!empty($this->getEmail()) && !is_null($this->getEmail())){
			$data['email'] = $this->getEmail();
		}

		return $data;
	}

	/**
	 * @return $this
	 * @throws UnauthenticatedException|InvalidValueException|NotFoundException|GuzzleException
	 */
	public function create():self {
		self::verifyCredentials();
		$this->validateCreateParams();
		$this->setAccounts($this->getValidatedAccountsArray());

		$request = new Request();
		$responseApi = $request->request(
			methodHttp:'POST',
			url: self::$baseUrl."payer",
			accessToken: self::$accessToken,
			accessCpfCnpj: self::$accessCpfCnpj,
			body:  $this->jsonSerialize()
		);

		$accounts = [];
		if(count($responseApi->accounts) > 0) {
			$accounts =	Account::fromArray($responseApi->accounts);
		}

		$this->setName($responseApi->name)
			->setEmail($responseApi->email)
			->setCpfCnpj($responseApi->cpfCnpj)
			->setAccounts($accounts)
			->setStreet($responseApi->street)
			->setNeighborhood($responseApi->neighborhood)
			->setAddressNumber($responseApi->addressNumber)
			->setAddressComplement($responseApi->addressComplement)
			->setCity($responseApi->city)
			->setState($responseApi->state)
			->setZipcode($responseApi->zipcode)
			->setToken($responseApi->token);

		return $this;
	}

	/**
	 * @param string $payerCpfCnpj
	 * @return $this
	 * @throws InvalidValueException
	 * @throws NotFoundException
	 * @throws UnauthenticatedException|GuzzleException
	 */
	public function search(string $payerCpfCnpj):self{
		self::verifyCredentials();
		$request = new Request();
		$responseApi = $request->request(
			methodHttp:'GET',
			url: self::$baseUrl."payer",
			accessToken: self::$accessToken,
			accessCpfCnpj: self::$accessCpfCnpj,
			headers: ['payercpfcnpj' => $payerCpfCnpj]
		);

		$accounts = [];
		if(count($responseApi->accounts) > 0) {
			$accounts =	Account::fromArray($responseApi->accounts);
		}

		$this->setName($responseApi->name)
			->setStatus($responseApi->status)
			->setAccounts($accounts)
			->setStreet($responseApi->street)
			->setNeighborhood($responseApi->neighborhood)
			->setCity($responseApi->city)
			->setState($responseApi->state)
			->setZipcode($responseApi->zipcode)
			->setToken($responseApi->token);

		return $this;
	}

	/**
	 * @param string $payerCpfCnpj
	 * @return $this
	 * @throws GuzzleException
	 * @throws InvalidValueException
	 * @throws NotFoundException
	 * @throws UnauthenticatedException
	 */
	public function update(string $payerCpfCnpj):self{
		self::verifyCredentials();

		$request = new Request();
		$responseApi = $request->request(
			methodHttp:'PUT',
			url: self::$baseUrl."payer",
			accessToken: self::$accessToken,
			accessCpfCnpj: self::$accessCpfCnpj,
			headers: ['payercpfcnpj' => $payerCpfCnpj],
			body: $this->getUpdatedParams()
		);


		$this->setName($responseApi->name)
			->setEmail($responseApi->email)
			->setStreet($responseApi->street)
			->setNeighborhood($responseApi->neighborhood)
			->setAddressNumber($responseApi->addressNumber)
			->setAddressComplement($responseApi->addressComplement)
			->setCity($responseApi->city)
			->setState($responseApi->state)
			->setZipcode($responseApi->zipcode);

		return $this;
	}

	/**
	 * @return void
	 * @throws InvalidValueException
	 */
	private function validateCreateParams():void{
		$requiredFields = ["name", "cpfCnpj", "accounts", "neighborhood", "city", "state", "zipcode"];

		foreach ($requiredFields as $field){
			if($this->{$field} === null) {
				throw new InvalidValueException(message: "$field field is mandatory");
			}
		}
	}

	/**
	 * @return array
	 */
	private function getUpdatedParams():array{
		$possibleFieldsUpdate = ["name", "email", "street", "neighborhood", "addressNumber", "addressComplement", "city", "state", "zipcode"];
		$updatedFields = [];
		foreach ($possibleFieldsUpdate as  $field){
			if($this->{$field} !== null){
				$updatedFields += [$field => $this->{$field}];
			}
		}
		return $updatedFields;
	}

	/**
	 * @return array
	 * @throws InvalidValueException
	 */
	private function getValidatedAccountsArray():array{
		if($this->getAccounts() !== null){
			$serializedAccountsArray =[];
			foreach ($this->getAccounts() as $account){

				if (!$account instanceof Account){
					throw new InvalidValueException("Invalid type account. Expected an array of "  .Account::class." found " . is_object($account) ? $account::class : gettype($account));
				}
				$account->validatePayerAccountsRequiredFields();

				$serializedAccountsArray[] = $account->jsonSerialize();
			}

			return $serializedAccountsArray;
		}

		return [];
	}

}