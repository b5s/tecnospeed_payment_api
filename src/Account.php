<?php

namespace B5STecnologia\TecnospeedPaymentAPI;

use B5STecnologia\TecnospeedPaymentAPI\Infra\Exceptions\InvalidValueException;
use B5STecnologia\TecnospeedPaymentAPI\Infra\Exceptions\UnauthenticatedException;
use GuzzleHttp\Exception\GuzzleException;
use JsonSerializable;

class Account  extends Tecnospeed implements JsonSerializable {
	/**
	 * @param string|null $bankCode
	 * @param string|null $agency
	 * @param string|null $agencyDigit
	 * @param string|null $accountNumber
	 * @param string|null $accountNumberDigit
	 * @param int|string|null $convenioAgency
	 * @param string|null $convenioNumber
	 * @param int|null $remessaSequential
	 * @param string|null $accountHash
	 * @param bool|null $van
	 * @param string|null $accountDac
	 */
	public function __construct(
		private ?string $bankCode = null,
		private ?string $agency = null,
		private ?string $agencyDigit = null,
		private ?string $accountNumber = null,
		private ?string $accountNumberDigit = null,
		private int|string|null $convenioAgency = null,
		private ?string $convenioNumber = null,
		private ?int $remessaSequential = null,
		private ?string $accountHash = null,
		private ?bool $van = null,
		private ?string $accountDac = null
	){

	}

	/**
	 * @return string|null
	 */
	public function getBankCode(): ?string
	{
		return $this->bankCode;
	}

	/**
	 * @param string|null $bankCode
	 * @return $this
	 */
	public function setBankCode(?string $bankCode):self
	{
		$this->bankCode = $bankCode;
		return $this;
	}

	/**
	 * @return string|null
	 */
	public function getAgency(): ?string
	{
		return $this->agency;
	}

	/**
	 * @param string|null $agency
	 * @return $this
	 */
	public function setAgency(?string $agency):self
	{
		$this->agency = $agency;
		return $this;
	}

	/**
	 * @return string|null
	 */
	public function getAgencyDigit(): ?string
	{
		return $this->agencyDigit;
	}

	/**
	 * @param string|null $agencyDigit
	 * @return $this
	 */
	public function setAgencyDigit(?string $agencyDigit):self
	{
		$this->agencyDigit = $agencyDigit;
		return $this;
	}

	/**
	 * @return string|null
	 */
	public function getAccountNumber(): ?string
	{
		return $this->accountNumber;
	}

	/**
	 * @param string|null $accountNumber
	 * @return $this
	 */
	public function setAccountNumber(?string $accountNumber):self
	{
		$this->accountNumber = $accountNumber;
		return $this;
	}

	/**
	 * @return string|null
	 */
	public function getAccountNumberDigit(): ?string
	{
		return $this->accountNumberDigit;
	}

	/**
	 * @param string|null $accountNumberDigit
	 * @return $this
	 */
	public function setAccountNumberDigit(?string $accountNumberDigit):self
	{
		$this->accountNumberDigit = $accountNumberDigit;
		return $this;
	}

	/**
	 * @return int|string|null
	 */
	public function getConvenioAgency(): int|string|null
	{
		return $this->convenioAgency;
	}

	/**
	 * @param int|string|null $convenioAgency
	 * @return $this
	 */
	public function setConvenioAgency(int|string|null $convenioAgency):self
	{
		$this->convenioAgency = $convenioAgency;
		return $this;
	}

	/**
	 * @return string|null
	 */
	public function getConvenioNumber(): ?string
	{
		return $this->convenioNumber;
	}

	/**
	 * @param string|null $convenioNumber
	 * @return $this
	 */
	public function setConvenioNumber(?string $convenioNumber):self
	{
		$this->convenioNumber = $convenioNumber;
		return $this;
	}

	/**
	 * @return int|null
	 */
	public function getRemessaSequential(): ?int
	{
		return $this->remessaSequential;
	}

	/**
	 * @param int|null $remessaSequential
	 * @return $this
	 */
	public function setRemessaSequential(?int $remessaSequential):self
	{
		$this->remessaSequential = $remessaSequential;
		return $this;
	}

	/**
	 * @return string|null
	 */
	public function getAccountHash(): ?string
	{
		return $this->accountHash;
	}

	/**
	 * @param string|null $accountHash
	 * @return $this
	 */
	public function setAccountHash(?string $accountHash):self
	{
		$this->accountHash = $accountHash;
		return $this;
	}

	/**
	 * @return bool|null
	 */
	public function getVan(): ?bool
	{
		return $this->van;
	}

	/**
	 * @param bool|null $van
	 * @return $this
	 */
	public function setVan(?bool $van):self
	{
		$this->van = $van;
		return $this;
	}

	/**
	 * @return string|null
	 */
	public function getAccountDac(): ?string
	{
		return $this->accountDac;
	}

	/**
	 * @param string|null $accountDac
	 * @return $this
	 */
	public function setAccountDac(?string $accountDac):self
	{
		$this->accountDac = $accountDac;
		return $this;
	}



	/**
	 * @param string $payerCpfCnpj
	 * @param array $accounts
	 * @return array
	 * @throws Infra\Exceptions\NotFoundException
	 * @throws InvalidValueException
	 * @throws UnauthenticatedException
	 * @throws GuzzleException
	 */
	public function create(string $payerCpfCnpj, array $accounts):array{
		self::verifyCredentials();
		$newAccounts = $this->getValidatedData($accounts);

		$request = new Request();
		$responseApi = $request->request(
			methodHttp:'POST',
			url: self::$baseUrl."account",
			accessToken: self::$accessToken,
			accessCpfCnpj: self::$accessCpfCnpj,
			headers: ['payerCpfCnpj' => $payerCpfCnpj],
			body:  $newAccounts
		);
		return ["accounts" => self::fromArray($responseApi->accounts)];
	}

	/**
	 * @param string $payerCpfCnpj
	 * @return array
	 * @throws GuzzleException
	 * @throws Infra\Exceptions\NotFoundException
	 * @throws InvalidValueException
	 * @throws UnauthenticatedException
	 */
	public function search(string $payerCpfCnpj):array{
		self::verifyCredentials();
		$request = new Request();
		$responseApi = $request->request(
			methodHttp:'GET',
			url: self::$baseUrl."account",
			accessToken: self::$accessToken,
			accessCpfCnpj: self::$accessCpfCnpj,
			headers: ['payercpfcnpj' => $payerCpfCnpj]
		);

		return ["accounts" => self::fromArray($responseApi->accounts)];

	}

	/**
	 * @param string $payerCpfCnpj
	 * @param string $accountHash
	 * @return $this
	 * @throws GuzzleException
	 * @throws Infra\Exceptions\NotFoundException
	 * @throws InvalidValueException
	 * @throws UnauthenticatedException
	 */
	public function update(string $payerCpfCnpj, string $accountHash):self{
		self::verifyCredentials();
		$request = new Request();
		$responseApi = $request->request(
			methodHttp:'PUT',
			url: self::$baseUrl."account/$accountHash",
			accessToken: self::$accessToken,
			accessCpfCnpj: self::$accessCpfCnpj,
			headers: ['payercpfcnpj' => $payerCpfCnpj],
			body: $this->returnNotNullUpdateParams()
		);

		$this->setBankCode($responseApi->bankCode)
			->setAccountHash($responseApi->accountHash)
			->setAgency($responseApi->agency)
			->setAgencyDigit($responseApi->agencyDigit)
			->setAccountNumber($responseApi->accountNumber)
			->setAccountNumberDigit($responseApi->accountNumberDigit)
			->setAccountDac($responseApi->accountDac)
			->setConvenioAgency($responseApi->convenioAgency)
			->setConvenioNumber($responseApi->convenioNumber)
			->setRemessaSequential($responseApi->remessaSequential);

		return $this;
	}

	/**
	 * @param string $payerCpfCnpj
	 * @param array $hashDeleteAccounts
	 * @return array
	 * @throws GuzzleException
	 * @throws Infra\Exceptions\NotFoundException
	 * @throws InvalidValueException
	 * @throws UnauthenticatedException
	 */
	public function delete(string $payerCpfCnpj, array $hashDeleteAccounts):array{
		self::verifyCredentials();
		$request = new Request();
		$responseApi = $request->request(
			methodHttp:'DELETE',
			url: self::$baseUrl."account",
			accessToken: self::$accessToken,
			accessCpfCnpj: self::$accessCpfCnpj,
			headers: ['payercpfcnpj' => $payerCpfCnpj],
			body: ['accountHash' => $hashDeleteAccounts]
		);

		return ["accounts" => self::fromArray($responseApi->accounts)];
	}

	/**
	 * @return array
	 */
	public function jsonSerialize():array
	{
		return [
			'bankCode' => $this->getBankCode(),
			'agency'   => $this->getAgency(),
			'agencyDigit' => $this->getAgencyDigit(),
			'accountNumber' => $this->getAccountNumber(),
			'accountNumberDigit' => $this->getAccountNumberDigit(),
			'convenioAgency' => $this->getConvenioAgency(),
			'convenioNumber' => $this->getConvenioNumber(),
			'remessaSequential' => $this->getRemessaSequential(),
			'accountHash' => $this->getAccountHash(),
			'van'   => $this->getVan() ?? false,
			'accountDac' => $this->getAccountDac()
		];
	}

	/**
	 * @return void
	 * @throws InvalidValueException
	 */
	private function validateCreateParams():void{
		$requiredFields = ["bankCode", "agency", "accountNumber"];

		foreach ($requiredFields as $field){
			if($this->{$field} === null) {
				throw new InvalidValueException(message: "$field field is mandatory");
			}
		}
	}

	/**
	 * @return array
	 */
	private function returnNotNullUpdateParams():array{
		$possibleFieldsUpdate = ["bankCode", "agency", "agencyDigit", "accountNumber", "accountNumberDigit", "convenioAgency", "convenioNumber", "remessaSequential", "van", "accountDac"];

		$updatedFields = [];
		foreach ($possibleFieldsUpdate as  $field){
			if($this->{$field} !== null){
				$updatedFields += [$field => $this->{$field}];
			}
		}
		return $updatedFields;
	}


	/**
	 * @throws InvalidValueException
	 */
	private function getValidatedData (array $accounts):array{

		$newAccounts = [];
		foreach ($accounts as $account){
			if (!$account instanceof self){
				throw new InvalidValueException("Invalid type account. Expected array of objects of type " .Account::class );
			}
			$account->validateCreateParams();
			$newAccounts[] = $account->jsonSerialize();
		}

		return $newAccounts;
	}

	public static function fromArray(array $accounts):array{
		$returnAccounts = [];
		foreach($accounts as $accountObject){
			$account = self::getNewAccount();
			$account->setBankCode($accountObject->bankCode)
				->setAccountHash($accountObject->accountHash)
				->setAgency($accountObject->agency)
				->setAgencyDigit($accountObject->agencyDigit)
				->setAccountNumber($accountObject->accountNumber)
				->setAccountNumberDigit($accountObject->accountNumberDigit)
				->setConvenioAgency($accountObject->convenioAgency ?? null)
				->setConvenioNumber($accountObject->convenioNumber)
				->setRemessaSequential($accountObject->remessaSequential)
				->setVan($accountObject->van ?? null)
				->setAccountDac($accountObject->accountDac);
			$returnAccounts[] = $account;
		}

		return $returnAccounts;
	}


	/**
	 * @return void
	 * @throws InvalidValueException
	 */
	public function validatePayerAccountsRequiredFields():void{
		$requiredFields = ["bankCode", "agency", "accountNumber", "accountDac", "convenioNumber"];
		foreach ($requiredFields as $field){
			if($this->{$field} === null) {
				throw new InvalidValueException(message: "$field field is mandatory");
			}
		}

	}
}