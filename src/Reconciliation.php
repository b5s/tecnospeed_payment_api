<?php

namespace B5STecnologia\TecnospeedPaymentAPI;



use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Psr7\Utils;

class Reconciliation extends Tecnospeed implements \JsonSerializable{


	/**
	 * @param string|null $uniqueId
	 * @param string|null $status
	 * @param string|null $source
	 * @param string|null $reason
	 * @param string|null $accountHash
	 * @param Payment[]|null $payments
	 * @param string|null $createdAt
	 * @param string|null $updatedAt
	 * @param string|null $content
	 */
	public function __construct(
		private ?string $uniqueId = null,
		private ?string $status = null,
		private ?string $source = null,
		private ?string $reason = null,
		private ?string $accountHash = null,
		private ?array $payments = null,
		private ?string $createdAt = null,
		private ?string $updatedAt = null,
		private ?string $content = null,
	){}

	/**
	 * @return string|null
	 */
	public function getUniqueId(): ?string
	{
		return $this->uniqueId;
	}

	/**
	 * @param string $uniqueId
	 * @return Reconciliation
	 */
	public function setUniqueId(string $uniqueId): Reconciliation
	{
		$this->uniqueId = $uniqueId;
		return $this;
	}

	/**
	 * @return string|null
	 */
	public function getStatus(): ?string
	{
		return $this->status;
	}

	/**
	 * @param string $status
	 * @return Reconciliation
	 */
	public function setStatus(string $status): Reconciliation
	{
		$this->status = $status;
		return $this;
	}

	/**
	 * @return string|null
	 */
	public function getSource(): ?string
	{
		return $this->source;
	}

	/**
	 * @param string $source
	 * @return Reconciliation
	 */
	public function setSource(string $source): Reconciliation
	{
		$this->source = $source;
		return $this;
	}

	/**
	 * @return string|null
	 */
	public function getReason(): ?string
	{
		return $this->reason;
	}

	/**
	 * @param string $reason
	 * @return Reconciliation
	 */
	public function setReason(string $reason): Reconciliation
	{
		$this->reason = $reason;
		return $this;
	}

	/**
	 * @return string|null
	 */
	public function getAccountHash(): ?string
	{
		return $this->accountHash;
	}

	/**
	 * @param string $accountHash
	 * @return Reconciliation
	 */
	public function setAccountHash(string $accountHash): Reconciliation
	{
		$this->accountHash = $accountHash;
		return $this;
	}

	/**
	 * @return Payment[]|null
	 */
	public function getPayments(): ?array
	{
		return $this->payments;
	}

	/**
	 * @param Payment[] $payments
	 * @return Reconciliation
	 */
	public function setPayments(array $payments): Reconciliation
	{
		$this->payments = $payments;
		return $this;
	}

	/**
	 * @return string|null
	 */
	public function getCreatedAt(): ?string
	{
		return $this->createdAt;
	}

	/**
	 * @param string $createdAt
	 * @return Reconciliation
	 */
	public function setCreatedAt(string $createdAt): Reconciliation
	{
		$this->createdAt = $createdAt;
		return $this;
	}

	/**
	 * @return string|null
	 */
	public function getUpdatedAt(): ?string
	{
		return $this->updatedAt;
	}

	/**
	 * @param string $updatedAt
	 * @return Reconciliation
	 */
	public function setUpdatedAt(string $updatedAt): Reconciliation
	{
		$this->updatedAt = $updatedAt;
		return $this;
	}

	/**
	 * @return string|null
	 */
	public function getContent(): ?string
	{
		return $this->content;
	}

	/**
	 * @param string $content
	 * @return Reconciliation
	 */
	public function setContent(string $content): Reconciliation
	{
		$this->content = $content;
		return $this;
	}

	public function jsonSerialize():array{
		return [
			"uniqueId" => $this->getUniqueId(),
			"status" => $this->getStatus(),
			"source" => $this->getSource(),
			"reason" => $this->getReason(),
			"accountHash" => $this->getAccountHash(),
			"payments" => $this->getPayments(),
			"createdAt" => $this->getCreatedAt(),
			"updatedAt" => $this->getUpdatedAt()
		];
	}

	/**
	 * @throws Infra\Exceptions\UnauthenticatedException
	 * @throws Infra\Exceptions\InvalidValueException
	 * @throws Infra\Exceptions\NotFoundException
	 * @throws GuzzleException
	 */
	public function sendReconciliationArchive(string $payerCpfCnpj, string $filename, string $uriFile):Reconciliation
	{
		self::verifyCredentials();
		$options = [
			'multipart' => [
				[
					'name' => 'file',
					'contents' => Utils::tryFopen($uriFile, 'r'),
					'filename' => $filename,
					'headers' => [
						'Content-Type' => '<Content-type header>'
					]
				]
			]
		];

		$request = new Request();
		$responseApi = $request->request(
			methodHttp: "POST",
			url: self::$baseUrl."reconciliation",
			accessToken: self::$accessToken,
			accessCpfCnpj: self::$accessCpfCnpj,
			headers: ['payercpfcnpj' => $payerCpfCnpj],
			options: $options
		);


		$this->setUniqueId($responseApi->uniqueId)
			->setStatus($responseApi->status)
			->setCreatedAt($responseApi->createdAt)
			->setUpdatedAt($responseApi->updatedAt);

		return $this;
	}

	/**
	 * @param string $payerCpfCnpj
	 * @param string $dateStart
	 * @param string $dateEnd
	 * @param int|null $page
	 * @param int|null $limit
	 * @return array
	 * @throws GuzzleException
	 * @throws Infra\Exceptions\InvalidValueException
	 * @throws Infra\Exceptions\NotFoundException
	 * @throws Infra\Exceptions\UnauthenticatedException
	 */
	public function getPerPeriod(string $payerCpfCnpj, string $dateStart, string $dateEnd, ?int $page = null, ?int $limit = null):array
	{

		$queryParams = "";
		if ($page !== null) {
			$queryParams .= "&page=" . $page;
		}

		if ($limit !== null) {
			$queryParams .= "&limit=" . $limit;
		}
		self::verifyCredentials();
		$request = new Request();
		$responseApi = $request->request(
			methodHttp: "GET",
			url: self::$baseUrl . "reconciliation?dateStart=" . $dateStart . "&dateEnd=" . $dateEnd . $queryParams,
			accessToken: self::$accessToken,
			accessCpfCnpj: self::$accessCpfCnpj,
			headers: ['payercpfcnpj' => $payerCpfCnpj]
		);
		$reconciliations = [];

		foreach($responseApi->data as $reconciliation){
			$newReconciliation = self::getNewReconciliation();
			$reconciliations[] = $newReconciliation->setUniqueId($reconciliation->uniqueid)
				->setCreatedAt($reconciliation->createdAt)->setSource($reconciliation->source);
		}

		return [
			"data" => 	$reconciliations,
			"count" => $responseApi->meta->count,
			"page" =>  $responseApi->meta->page,
			"totalPages" =>  $responseApi->meta->totalPages
		];
	}

	/**
	 * @param string $payerCpfCnpj
	 * @param string $uniqueId
	 * @return Reconciliation
	 * @throws GuzzleException
	 * @throws Infra\Exceptions\InvalidValueException
	 * @throws Infra\Exceptions\NotFoundException
	 * @throws Infra\Exceptions\UnauthenticatedException
	 */
	public function getByUniqueId(string $payerCpfCnpj, string $uniqueId):Reconciliation{
		self::verifyCredentials();
		$request = new Request();
		$responseApi = $request->request(
			methodHttp: "GET",
			url: self::$baseUrl."reconciliation/".$uniqueId,
			accessToken:self::$accessToken,
			accessCpfCnpj: self::$accessCpfCnpj,
			headers: ['payercpfcnpj' => $payerCpfCnpj]
		);

		$this->setUniqueId($responseApi->uniqueId)->setStatus($responseApi->status)->setReason($responseApi->reason);

		if(isset($responseApi->accountHash)){
			$this->setAccountHash($responseApi->accountHash);
		}

		if(isset($responseApi->payments) && count($responseApi->payments)){
			$this->setPayments(array_map(static function (\stdClass $returnedPayment){
				$payment = self::getNewPayment();
				return $payment->setUniqueId($returnedPayment->uniqueId)
					->setPaymentType($returnedPayment->paymentType)
					->setStatus($returnedPayment->status)
					->setTags($returnedPayment->tags)
					->setAuthenticationRegister($returnedPayment->authenticationRegister);
			}, $responseApi->payments));
		}
		return $this;
	}


	public function download(string $payerCpfCnpj, string $uniqueId):Reconciliation{
		self::verifyCredentials();
		$request = new Request();
		$responseApi = $request->request(
			methodHttp: "GET",
			url: self::$baseUrl."reconciliation/".$uniqueId."/download",
			accessToken:self::$accessToken,
			accessCpfCnpj: self::$accessCpfCnpj,
			headers: ['payercpfcnpj' => $payerCpfCnpj]
		);
		return $this->setContent($responseApi);
	}

}