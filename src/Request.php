<?php

namespace B5STecnologia\TecnospeedPaymentAPI;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\GuzzleException;
use B5STecnologia\TecnospeedPaymentAPI\Infra\Exceptions\NotFoundException;
use B5STecnologia\TecnospeedPaymentAPI\Infra\Exceptions\InvalidValueException;
use B5STecnologia\TecnospeedPaymentAPI\Infra\Exceptions\UnauthenticatedException;
class Request {

	/**
	 * @param string $methodHttp
	 * @param string $url
	 * @param string $accessToken
	 * @param string $accessCpfCnpj
	 * @param array $headers
	 * @param array|object $body
	 * @param array $options
	 * @return mixed
	 * @throws GuzzleException
	 * @throws InvalidValueException
	 * @throws NotFoundException
	 * @throws UnauthenticatedException
	 */
	public function request(string $methodHttp, string $url, string $accessToken, string $accessCpfCnpj, array $headers = [], array|object $body = [], array $options = []): mixed
	{
		$requestHeaders = [
			'cnpjsh' => $accessCpfCnpj,
			'tokensh' => $accessToken
		];

		if(!count($options)){
			$requestHeaders['Content-Type'] = 'application/json';
		}

		$requestHeaders = array_merge($requestHeaders, $headers);

		$options['verify'] = false;
		$options['headers'] = $requestHeaders;
		$options['body'] = json_encode($body);

		try {
			$client = new Client();
			$guzzleResponse = $client->request($methodHttp, $url, $options);

			if($guzzleResponse->getHeaders()["Content-Type"][0] !== "application/json; charset=utf-8"){
				return $guzzleResponse->getBody()->getContents();
			}

			return json_decode($guzzleResponse->getBody());

		}catch (ClientException $e){

			if ($e->getCode() === 404){
				throw new NotFoundException("The requested was not found", previous: $e);
			}

			if($e->getCode() === 401){
				throw new UnauthenticatedException("The informed access token/cnpj is incorrect",  previous: $e);
			}
			$error = json_decode($e->getResponse()->getBody()->getContents());
			throw new InvalidValueException($error->errors[0]->message, previous: $e);
		}
	}

}