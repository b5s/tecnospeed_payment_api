<?php

namespace B5STecnologia\TecnospeedPaymentAPI;


use GuzzleHttp\Exception\GuzzleException;

class Remittance extends Tecnospeed implements \JsonSerializable {
	/**
	 * @param array|null $payments
	 * @param string|null $uniqueId
	 * @param string|null $status
	 * @param string|null $remittanceType
	 * @param string|null $createdAt
	 * @param string|null $content
	 */
	public function __construct(
		private ?array $payments = null,
		private ?string $uniqueId = null,
		private ?string $status = null,
		private ?string $remittanceType = null,
		private ?string $createdAt = null,
		private ?string $content = null
	){}

	/**
	 * @return array|null
	 */
	public function getPayments(): ?array
	{
		return $this->payments;
	}

	/**
	 * @param array|null $payments
	 * @return Remittance
	 */
	public function setPayments(?array $payments): Remittance
	{
		$this->payments = $payments;
		return $this;
	}

	/**
	 * @return string|null
	 */
	public function getUniqueId(): ?string
	{
		return $this->uniqueId;
	}

	/**
	 * @param string|null $uniqueId
	 * @return Remittance
	 */
	public function setUniqueId(?string $uniqueId): Remittance
	{
		$this->uniqueId = $uniqueId;
		return $this;
	}

	/**
	 * @return string|null
	 */
	public function getStatus(): ?string
	{
		return $this->status;
	}

	/**
	 * @param string|null $status
	 * @return Remittance
	 */
	public function setStatus(?string $status): Remittance
	{
		$this->status = $status;
		return $this;
	}

	/**
	 * @return string|null
	 */
	public function getRemittanceType(): ?string
	{
		return $this->remittanceType;
	}

	/**
	 * @param string|null $remittanceType
	 * @return Remittance
	 */
	public function setRemittanceType(?string $remittanceType): Remittance
	{
		$this->remittanceType = $remittanceType;
		return $this;
	}

	/**
	 * @return string|null
	 */
	public function getCreatedAt(): ?string
	{
		return $this->createdAt;
	}

	/**
	 * @param string|null $createdAt
	 * @return Remittance
	 */
	public function setCreatedAt(?string $createdAt): Remittance
	{
		$this->createdAt = $createdAt;
		return $this;
	}

	/**
	 * @return string|null
	 */
	public function getContent(): ?string
	{
		return $this->content;
	}

	/**
	 * @param string|null $content
	 * @return Remittance
	 */
	public function setContent(?string $content): Remittance
	{
		$this->content = $content;
		return $this;
	}


	public function jsonSerialize():array{
		return [
			'uniqueId' => $this->getUniqueId(),
			'status'   => $this->getStatus(),
			'remittanceType' => $this->getRemittanceType(),
			'payments' => $this->getPayments(),
			'createdAt' => $this->getCreatedAt(),
			'content' => $this->getContent()
		];
	}

	/**
	 * @param string $payerCpfCnpj
	 * @param array $payments
	 * @return Remittance
	 * @throws GuzzleException
	 * @throws Infra\Exceptions\InvalidValueException
	 * @throws Infra\Exceptions\NotFoundException
	 * @throws Infra\Exceptions\UnauthenticatedException
	 */
	public function generate(string $payerCpfCnpj, array $payments):Remittance{
		self::verifyCredentials();
		$request = new Request();
		$responseApi = $request->request(
			methodHttp: "POST",
			url: self::$baseUrl."remittance",
			accessToken: self::$accessToken,
			accessCpfCnpj: self::$accessCpfCnpj,
			headers: ['payercpfcnpj' => $payerCpfCnpj],
			body: ["payments" => $payments]
		)[0];

		$payment = self::getNewPayment();
		$this->setUniqueId($responseApi->uniqueId)
			->setStatus($responseApi->status)
			->setRemittanceType($responseApi->remittanceType)
			->setPayments(array_map(static function (\stdClass $returnedPayment) use ($payment){
				return $payment->setUniqueId($returnedPayment->uniqueId)
					->setAccountHash($returnedPayment->accountHash)
					->setPaymentType($returnedPayment->paymentType)
					->setPaymentForm($returnedPayment->paymentForm);
			}, $responseApi->payments));

		return $this;
	}

	/**
	 * @param string $payerCpfCnpj
	 * @param string $dateStart
	 * @param string $dateEnd
	 * @param int|null $page
	 * @param int|null $limit
	 * @return array
	 * @throws GuzzleException
	 * @throws Infra\Exceptions\InvalidValueException
	 * @throws Infra\Exceptions\NotFoundException
	 * @throws Infra\Exceptions\UnauthenticatedException
	 */
	public function getPerPeriod(string $payerCpfCnpj, string $dateStart, string $dateEnd, ?int $page = null, ?int $limit = null):array{

		$queryParams = "";
		if($page !== null){
			$queryParams .= "&page=".$page;
		}

		if($limit !== null){
			$queryParams .= "&limit=".$limit;
		}
		self::verifyCredentials();
		$request = new Request();
		$responseApi = $request->request(
			methodHttp: "GET",
			url: self::$baseUrl."remittance?dateStart=".$dateStart."&dateEnd=".$dateEnd.$queryParams,
			accessToken:self::$accessToken,
			accessCpfCnpj: self::$accessCpfCnpj,
			headers: ['payercpfcnpj' => $payerCpfCnpj]
		);
		$remittances = [];

		foreach($responseApi->data as $remittance){
			$newRemittance = self::getNewRemittance();
			$remittances[] = $newRemittance->setUniqueId($remittance->uniqueid)
							->setCreatedAt($remittance->createdAt);
		}
		return [
			"data" => $remittances,
			"count" => $responseApi->meta->count,
			"page" =>  $responseApi->meta->page,
			"totalPages" =>  $responseApi->meta->totalPages
		];
	}

	/**
	 * @param string $payerCpfCnpj
	 * @param string $uniqueId
	 * @return Remittance
	 * @throws GuzzleException
	 * @throws Infra\Exceptions\InvalidValueException
	 * @throws Infra\Exceptions\NotFoundException
	 * @throws Infra\Exceptions\UnauthenticatedException
	 */
	public function getByUniqueId(string $payerCpfCnpj, string $uniqueId):Remittance{
		self::verifyCredentials();
		$request = new Request();
		$responseApi = $request->request(
			methodHttp: "GET",
			url: self::$baseUrl."remittance/".$uniqueId,
			accessToken:self::$accessToken,
			accessCpfCnpj: self::$accessCpfCnpj,
			headers: ['payercpfcnpj' => $payerCpfCnpj]
		);


		$payment = self::getNewPayment();
		return $this->setStatus($responseApi->status)
			->setUniqueId($uniqueId)
			->setRemittanceType($responseApi->remittanceType)
			->setPayments(array_map(static function (\stdClass $returnedPayment) use ($payment){
				return $payment->setUniqueId($returnedPayment->uniqueId)
					->setAccountHash($returnedPayment->accountHash)
					->setPaymentType($returnedPayment->paymentType)
					->setPaymentForm($returnedPayment->paymentForm);
			}, $responseApi->payments))
			->setContent($responseApi->content ?? null);
	}

	/**
	 * @param string $payerCpfCnpj
	 * @param string $uniqueId
	 * @return $this
	 * @throws GuzzleException
	 * @throws Infra\Exceptions\InvalidValueException
	 * @throws Infra\Exceptions\NotFoundException
	 * @throws Infra\Exceptions\UnauthenticatedException
	 */
	public function download(string $payerCpfCnpj, string $uniqueId):Remittance{
		self::verifyCredentials();
		$request = new Request();
		$responseApi = $request->request(
			methodHttp: "GET",
			url: self::$baseUrl."remittance/".$uniqueId."/download",
			accessToken:self::$accessToken,
			accessCpfCnpj: self::$accessCpfCnpj,
			headers: ['payercpfcnpj' => $payerCpfCnpj]
		);
		return $this->setContent($responseApi);
	}

}